# Contact and citation

## Contact
    Marek Zmeškal
    Faculty of Nuclear Sciences and Physical Engineering CTU in Prague
    Department of Nuclear Reactors
    180 OO Prague (Czech Republic)
    Contact: zmeskma1@fjfi.cvut.cz
    
    Loïc Thulliez
    CEA-Saclay
    DRF/Irfu/DPhN/LEARN
    91191 Gif-sur-Yvette (FRANCE)
    Contact: loic.thulliez@cea.fr

## Citation

If you use this code in anyway in your work please use the reference below since substantial efforts went into developping this code:  

   * M. Zmeškal, L. Thulliez, E. Dumonteil, Improvement of Geant4 Neutron-HP package: Doppler broadening of the neutron elastic scattering kernel and cross sections, Annals of Nuclear Energy, Volume 192, 2023, 109949, ISSN 0306-4549, https://doi.org/10.1016/j.anucene.2023.109949.
   * ArXiv: https://arxiv.org/abs/2303.07300

# Program Description
Application to benchmark the Geant4 Neutron-HP package.
It aims to test the low energy neutron transport (below 20 MeV) accuracy in Geant4.

The tests are done through comparison with neutron transport reference codes Tripoli-4 (version 11) and MCNP6.2 for materials gathered in a database. 
Two different benchmarks are performed [1]:
   * An integral (macroscopic) benchmark / The sphere benchmark :

        Goal: benchmark the slowing down process of the neutrons in Geant4.

        Geometry: a homogeneous sphere with radius of 30 cm.

        Neutron source:  an isotropic 10 eV mono-energetic point source is placed at the sphere center. 

        Score: the neutron flux in the sphere. 
            
   * A differential (microscopic) benchmark / The thin cylinder benchmark :

     Goal: benchmark the nuclear data treatment in Geant4.

     Geometry: a thin cylinder with a length of 2 meters and a radius of 1 um, these dimensions ensures that at least one and only one neutron interaction happens in the cylinder.

     Neutron source: the neutrons have an energy equal to 0.01 eV and are shot parallel to the cylinder axis.

     Score: the energy and cosine of scattering angle of the scattered neutron. 

[1] L. Thulliez, C. Jouanne, E. Dumonteil, Improvement of Geant4 Neutron-HP package: From methodology to evaluated nuclear data library, Nuclear Instruments and Methods in Physics Research Section A: Accelerators, Spectrometers, Detectors and Associated Equipment, Volume 1027, 2022, 166187, ISSN 0168-9002, https://doi.org/10.1016/j.nima.2021.166187.

# Prerequisites

1) Build and install the Geant4 version that you want to test.
2) Download the geant4_neutronhp_benchmarkapp project.
3) Python3 requirements: numpy (handling data), matplotlib in version 3.6 or higher (creating graphs) and uproot. These can be installed by running a command in ./geant4_neutronhp_benchmarkapp/Apps/Scripts directory:

        pip install -r requirements.txt
4) Latex requirements (to generate the benchmark summary): pdflatex and bibtex.

# Structure of the app

## ./geant4_neutronhp_benchmarkapp/Apps
Macroscopic and microscopic applications to benchmark the Geant4 Neutron-HP package and also a Microscopic application to benchmark new DBRC method in Geant4.

## ./geant4_neutronhp_benchmarkapp/Database
There are sub-directories for each of three compared simulation programs Geant4, MCNP6.2 and Tripoli-4. 
In MCNP6.2 and Tripoli-4 sub-directories there are directly the Micro and Macro sub-directories with data for these benchmarks.

In Geant4 directory a sub-directory for a given version is created and inside them again the Macro and Micro sub-directories are created. In Macro subdirectory there is one more sub-directory for the radius of the sphere (all data are calculated for 30 cm to this end). Then inside of these directories a sub-directory for each of material from the database is created. There you can find results of the simulation in Results.root, output from simulation in output.txt, myrun.mac macro file with which the simulation was run, graphs for comparison of Geant4, Tripoli4 and MCNP6.2 and statistics report, which is also summarized in final report. The data for Geant4 version 11.00.03 calculated in G4NDL45_ENDFB71_HP and G4NDL47_ENDFB80_HP are already present.

## ./geant4_neutronhp_benchmarkapp/Scripts

In the main directory, there are the main run scripts runner.py and process.py with their argument files runnerarguments.txt and processarguments.txt.
In the sub-directory modules, there are different modules used in process.py.
In sub-directory auxiliary, there are auxiliary files used in the main scripts, these are:

* energyGroup_616.txt : default energy grouping for energy flux and energy of the scattered neutron

* librarydatabase.json : database of libraries, which were input in runner and are stored here for each version of Geant4 to be used by process.py

* materialdabase.json : database of materials, for which data calculated in Tripoli-4 and MCNP6.2 are present

# Run the TSL benchmarks

## Step1: Build application and run simulations

1) Go to sub-directory ./geant4_neutronhp_benchmarkapp/
Scripts and prepare your runnerarguments.txt where the default values are set.
It is important to check the version, TSLreference, NeutronHPdatafile and TSLGeant4.
For description of the arguments run:

         python3 runner.py -h
2) Set the path in geant4.sh to the NeutronHPdatafile you declared as argument, set proper link for ThermalScattering directory you declared as argument.
3) Build applications and run the simulations for all materials in database in ./geant4_neutronhp_benchmarkapp/Scripts sub-directory:

         python3 runner.py @runnerarguments.txt
         
If you want to run a subset of material change the file ./geant4_neutronhp_benchmarkapp/Scripts/auxiliary/matarialdatabase.json

## Step 2: Generate the final report related to the benchmark
1) Check arguments in ./geant4_neutronhp_benchmarkapp/Scripts/processarguments.txt.
2) Run the processing script in ./geant4_neutronhp_benchmarkapp/Scripts sub-directory:

	    python3 process.py @processarguments.txt
3) The report is created in the sub-directory ./geant4_neutronhp_benchmarkapp/Database/Geant4/Geant4.version/  

Other graphs and statistics report will be saved to individual simulation directories.

# Run the DBRC benchmarks

## Step1: Build application, run simulations and generate the final report

1) Go to sub-directory ./geant4_neutronhp_benchmarkapp/
Scripts and prepare your argumentsDBRC.txt where the default values are set.
It is important to check the version and NeutronHPdatafile.
For description of the arguments run:

         python3 processDBRC.py -h
2) Set the path in geant4.sh to the NeutronHPdatafile you declared as argument.
3) Build application, run the simulations and prepare the final report in ./geant4_neutronhp_benchmarkapp/Scripts sub-directory:

         python3 processDBRC.py @argumentsDBRC.txt
         
So far only data for Uranium 238 (energies 6.52 eV, 20.2 eV and 36.25 eV and temperatures 300 K, 600 K and 1000 K) are present. The created report can be found in ./geant4_neutronhp_benchmarkapp/Database/Geant4/Geant4.version/DBRC.


## Acknowledgment
This work could be done thanks to the IAEA´s technical cooperation programme, which supported a fellowship of Marek Zmeskal at IRFU, CEA in the framework of national project CZR0011 - Strengthening Human Resources Capacity in Nuclear Science and Technology.
The authors wish to sincerely thank Alberto Ribon head of the Geant4 Hadronic group for fruitful discussions. 



