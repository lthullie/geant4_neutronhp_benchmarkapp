//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file DetectorMessenger.cc
/// \brief Implementation of the DetectorMessenger class
//
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "DetectorMessenger.hh"

#include "DetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcommand.hh"
#include "G4UIparameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4NistManager.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorMessenger::DetectorMessenger(DetectorConstruction * Det)
:G4UImessenger(),
 fDetector(Det), fTestemDir(0), fDetDir(0), fMaterCmd(0), fSizeCmd(0),
 fIsotopeCmd(0),fMatSpecCmd(0)
{
  fTestemDir = new G4UIdirectory("/testhadr/");
  fTestemDir->SetGuidance("commands specific to this example");

  G4bool broadcast = false;
  fDetDir = new G4UIdirectory("/testhadr/det/",broadcast);
  fDetDir->SetGuidance("detector construction commands");

  fMaterCmd = new G4UIcmdWithAString("/testhadr/det/setMat",this);
  fMaterCmd->SetGuidance("Select material of the sphere.");
  fMaterCmd->SetParameterName("choice",false);
  fMaterCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

/*  fTempCmd = new G4UIcmdWithADoubleAndUnit("/testhadr/det/setTemperature",this);
  fTempCmd->SetGuidance("Set temperature of the TS material");
  fTempCmd->SetParameterName("Temperature",false);
  fTempCmd->SetRange("Temperature>0.");
  fTempCmd->SetUnitCategory("Temperature");
  fTempCmd->AvailableForStates(G4State_PreInit);

  fPresCmd = new G4UIcmdWithADoubleAndUnit("/testhadr/det/setPressure",this);
  fPresCmd->SetGuidance("Set pressure of the TS material");
  fPresCmd->SetParameterName("Pressure",false);
  fPresCmd->SetRange("Pressure>0.");
  fPresCmd->SetUnitCategory("Pressure");
  fPresCmd->AvailableForStates(G4State_PreInit);*/

  fSizeCmd = new G4UIcmdWithADoubleAndUnit("/testhadr/det/setSize",this);
  fSizeCmd->SetGuidance("Set radius of the sphere");
  fSizeCmd->SetParameterName("Size",false);
  fSizeCmd->SetRange("Size>0.");
  fSizeCmd->SetUnitCategory("Length");
  fSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  fIsotopeCmd = new G4UIcommand("/testhadr/det/setIsotopeMat",this);
  fIsotopeCmd->SetGuidance("Build and select a material with single isotope");
  fIsotopeCmd->SetGuidance("  symbol of isotope, Z, A, density of material");
  //
  G4UIparameter* symbPrm = new G4UIparameter("isotope",'s',false);
  symbPrm->SetGuidance("isotope symbol");
  fIsotopeCmd->SetParameter(symbPrm);
  //
  G4UIparameter* ZPrm = new G4UIparameter("Z",'i',false);
  ZPrm->SetGuidance("Z");
  ZPrm->SetParameterRange("Z>0");
  fIsotopeCmd->SetParameter(ZPrm);
  //
  G4UIparameter* APrm = new G4UIparameter("A",'i',false);
  APrm->SetGuidance("A");
  APrm->SetParameterRange("A>0");
  fIsotopeCmd->SetParameter(APrm);
  //
  G4UIparameter* densityPrm = new G4UIparameter("density",'d',false);
  densityPrm->SetGuidance("density of material");
  densityPrm->SetParameterRange("density>0.");
  fIsotopeCmd->SetParameter(densityPrm);
  //
  G4UIparameter* unitPrm = new G4UIparameter("unit",'s',false);
  unitPrm->SetGuidance("unit of density");
  G4String unitList = G4UIcommand::UnitsList(G4UIcommand::CategoryOf("g/cm3"));
  unitPrm->SetParameterCandidates(unitList);
  fIsotopeCmd->SetParameter(unitPrm);
  //
  fIsotopeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  fMatSpecCmd = new G4UIcommand("/testhadr/det/setMatSpecial",this);
  fMatSpecCmd->SetGuidance("Build and select a material with a special");
  fMatSpecCmd->SetGuidance(" density, temperature and pressure.");
  //
  G4UIparameter* namePrm = new G4UIparameter("name of material",'s',false);
  namePrm->SetGuidance("material name");
  fMatSpecCmd->SetParameter(namePrm);
  //
  G4UIparameter* densitymatPrm = new G4UIparameter("density",'d',false);
  densitymatPrm->SetGuidance("density of material");
  densitymatPrm->SetParameterRange("density>0");
  fMatSpecCmd->SetParameter(densitymatPrm);
  //
  G4UIparameter* unitDensPrm = new G4UIparameter("unitDens",'s',false);
  unitDensPrm->SetGuidance("unit of density");
  G4String unitDensList = G4UIcommand::UnitsList("Volumic Mass");
  unitDensPrm->SetParameterCandidates(unitDensList);
  fMatSpecCmd->SetParameter(unitDensPrm);
  //
  G4UIparameter* temperaturePrm = new G4UIparameter("temperature",'d',false);
  temperaturePrm->SetGuidance("temperature of material");
  temperaturePrm->SetParameterRange("temperature>0");
  fMatSpecCmd->SetParameter(temperaturePrm);
  //
  G4UIparameter* unitTempPrm = new G4UIparameter("unitTemp",'s',false);
  unitTempPrm->SetGuidance("unit of temperature");
  G4String unitTempList = G4UIcommand::UnitsList("Temperature");
  unitTempPrm->SetParameterCandidates(unitTempList);
  fMatSpecCmd->SetParameter(unitTempPrm);
  //
  G4UIparameter* pressurePrm = new G4UIparameter("pressure",'d',false);
  pressurePrm->SetGuidance("pressure of material");
  pressurePrm->SetParameterRange("pressure>0");
  fMatSpecCmd->SetParameter(pressurePrm);
  //
  G4UIparameter* unitPresPrm = new G4UIparameter("unit",'s',false);
  unitPresPrm->SetGuidance("unit of pressure");
  G4String unitPresList = G4UIcommand::UnitsList("Pressure");
  unitPresPrm->SetParameterCandidates(unitPresList);
  fMatSpecCmd->SetParameter(unitPresPrm);
  //
  fMatSpecCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorMessenger::~DetectorMessenger()
{
  delete fMaterCmd;
  delete fSizeCmd;
  delete fIsotopeCmd;
  delete fMatSpecCmd;
  delete fDetDir;
  delete fTestemDir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command == fMaterCmd )
   { fDetector->SetMaterial(newValue);}

  /*if( command == fTempCmd )
    { fDetector->SetTemperature(fTempCmd->GetNewDoubleValue(newValue));}

  if( command == fPresCmd )
   { fDetector->SetPressure(fPresCmd->GetNewDoubleValue(newValue));}*/

  if( command == fSizeCmd )
   { fDetector->SetSize(fSizeCmd->GetNewDoubleValue(newValue));}

  if (command == fIsotopeCmd)
   {
     G4int Z; G4int A; G4double dens;
     G4String name, unt;
     std::istringstream is(newValue);
     is >> name >> Z >> A >> dens >> unt;
     dens *= G4UIcommand::ValueOf(unt);
     fDetector->MaterialWithSingleIsotope (name,name,dens,Z,A);
     fDetector->SetMaterial(name);
   }

   if (command == fMatSpecCmd)
    {
      G4double density, temperature, pressure;
      G4String basename, densunit, tempunit, presunit;
      std::istringstream is(newValue);
      is >> basename >> density >> densunit >> temperature >> tempunit >>
      pressure >> presunit;
      density *= G4UIcommand::ValueOf(densunit);
      temperature *= G4UIcommand::ValueOf(tempunit);
      pressure *= G4UIcommand::ValueOf(presunit);
      G4String newname=basename+"_altered";
      G4NistManager::Instance()->BuildMaterialWithNewDensity(newname,basename,
        density,temperature,pressure);
      fDetector->SetMaterial(newname);
    }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
