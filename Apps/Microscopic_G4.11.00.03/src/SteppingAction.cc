//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SteppingAction.cc
/// \brief Implementation of the SteppingAction class

#include "SteppingAction.hh"

#include "G4Step.hh"
#include "G4RunManager.hh"
#include "G4AnalysisManager.hh"

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction()
: G4UserSteppingAction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* aStep)
{
// Collect energy and track length step by step

const G4StepPoint* endPoint = aStep->GetPostStepPoint();
//const G4StepPoint* prePoint = aStep->GetPreStepPoint();
//G4double E_prim             = prePoint->GetKineticEnergy();
//G4ThreeVector p_prim = prePoint->GetMomentum();
//G4cout << "Primary energy is" << G4BestUnit(E_prim,"Energy") << G4endl;
//G4cout << "Primary mommentum is " << p_prim << G4endl;
G4AnalysisManager* analysis = G4AnalysisManager::Instance();
if (aStep->GetTrack()->GetTrackStatus() == fAlive) {
  G4double E_sec = endPoint->GetKineticEnergy();
  //G4cout << "Outgoing energy is" << G4BestUnit(E_sec,"Energy") << G4endl;
  analysis->FillH1(0,E_sec);
  G4ThreeVector p_sec = endPoint->GetMomentum();
  //G4cout << "Secondary mommentum is " << p_sec << G4endl;
  G4double cosTheta = p_sec.cosTheta();
  //G4cout << "The cosine of angle is " << cosTheta << G4endl;
  analysis->FillH1(1,cosTheta);
}

G4RunManager::GetRunManager()->AbortEvent();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
