import numpy as np
import json


def createtitlepage(path,version):
    doc=open(path+"/Geant4/Geant4."+version+"/Geant4."+version+"NeutronHPBenchmark.tex", "w")
    doc.write("\\documentclass{report}\n")
    doc.write("\\usepackage[T1]{fontenc}\n")
    doc.write("\\usepackage[utf8]{inputenc}\n")
    doc.write("\\usepackage{lmodern}\n")
    doc.write("\\usepackage{textcomp}\n")
    doc.write("\\usepackage{listings}\n")
    doc.write("\\usepackage{geometry}\n")
    doc.write("\\geometry{tmargin=2cm,lmargin=1.5cm,rmargin=1.5cm,bmargin=2cm}\n")
    doc.write("\\usepackage{hyperref}\n")
    doc.write("\\usepackage{graphicx}\n")
    doc.write("\\usepackage{amsmath}\n")
    doc.write("\\usepackage{etoolbox}\n")
    doc.write("\\patchcmd{\\abstract}{\\titlepage}{}{}{}\n")
    doc.write("\\patchcmd{\\endabstract}{\\endtitlepage}{}{}\n")
    doc.write("\n")
    doc.write("\\title{Geant4."+version+" NeutronHP Benchmark}\n")
    doc.write(r"""\author{Marek Zmeskal$^{a,b,*}$, Loic Thulliez$^{c}$, Eric Dumonteil$^{c}$ \\
    \\
    $^a$\textit{Dept. of Nuclear Reactors, Faculty of Nuclear Sciences and Physical Engineering,}\\
    $^b$\textit{Research Centre Rez, Hlavni 130, 250 68 Husinec-Rez, Czech Republic}\\
    \textit{Czech Technical University in Prague, V Holesovickach 2, 180 00 Prague, Czech Republic}\\
    $^c$\textit{IRFU, CEA, Université Paris{-}Saclay, 911 91 Gif{-}sur{-}Yvette, France}\\
    \\
    $^*$\textit{email: zmeskma1@fjfi.cvut.cz} }"""+"\n")
    doc.write(r"""\date{Report created on\\
    \today \\
    with the version of code and database from \\
    February 21, 2023}"""+"\n")
    doc.write("\\begin{document}\n")
    #doc.write("\\normalsize\n")
    doc.write("\\maketitle\n")
    doc.write("\\begin{titlepage}\n")
    doc.write("\\begin{abstract}\n")
    doc.write("This report shows comparison of Geant4 thermal scattering law with reference codes Tripoli-4 (version 11) and MCNP6.2. This report can be used to ensure proper TSL treatment in all future versions of Geant4 and also to compare thermal scattering for different materials and in different nuclear data libraries.\n")
    doc.write("\\end{abstract}\n")
    doc.write("\\renewcommand{\\abstractname}{Acknowledgements}\n")
    doc.write("\\begin{abstract}\n")
    doc.write("This work could be done thanks to the IAEA´s technical cooperation programme, which supported a fellowship of Marek Zmeskal at IRFU, CEA in the framework of national project CZR0011 - Strengthening Human Resources Capacity in Nuclear Science and Technology. The authors wish to sincerely thank Alberto Ribon, head of the Geant4 Hadronic group, for fruitful discussions. Tripoli-4 is a registered trademark of CEA. \n")
    doc.write("\\end{abstract}\n")
    doc.write("\\end{titlepage}\n")
    doc.write("\\tableofcontents\n")
    doc.write("\n")
    return doc

def createheaderpage(mac_or_mic,doc,path,exponent,radius=30):
    if mac_or_mic=="Macroscopic":
        doc.write("\\chapter{"+mac_or_mic+"}\n")
        doc.write("This model is aimed at the description of slowing down and thermalization and the description of the transition between the free gas approximation energy range above 4 eV and the thermal scattering law energy range below this threshold.\\\\\n")
        doc.write("Macroscopic benchmark consists of a sphere with "+str(radius)+" cm radius.\\\\\n")
        doc.write("The source of neutrons is an isotropic monoenergetic point source in the center of the sphere.\\\\\n")
        doc.write("The simulation was run with 1E8 particles in Tripoli-4 and MCNP6.2 and with 1E"+str(exponent)+" particles in Geant4.\\\\\n")
        doc.write("The scored and plotted quantity is a neutron flux inside the sphere normalized to one source particle.\\\\\n")
        doc.write("\\begin{figure}[h!]\n")
        doc.write("\\centering\n")
        doc.write("\\includegraphics[height=11 cm]{"+path+"/auxiliary/macro.png}\n")
        doc.write("\\caption{Model of the macroscopic benchmark with 50 primary particles.}\n")
        doc.write("\\end{figure}\n")
    if mac_or_mic=="Microscopic":
        doc.write("\\chapter{"+mac_or_mic+"}\n")
        doc.write("This model is aimed at the precise description of the spectral and angular characteristics of individual scatterings. It checks the inelastic coherent and incoherent elastic cross sections.\\\\\n")
        doc.write(r"Microscopic benchmark consists of a thin cylinder with radius of 1 $\mu$m and length 2 m.\\"+"\n")
        doc.write("The source of neutrons is monoenergetic point\
         source with direction parallel to axis z with direction in +z and placed at the -z end of the cylinder.\\\\\n")
        doc.write("The simulation was run with 1E8 particles in Tripoli-4 and MCNP6.2. and with 1E"+str(exponent)+" particles in Geant4.\\\\\n")
        doc.write("The scored and plotted quantities are energy of the scattered neutron and its scattering angle after the first collision. The result is normalized to 1E8 particles.\\\\\n")
        doc.write("There is a known discrepancy in description of scattering in forward angles in MCNP6 \\cite{TRAN201884}, which is caused by the angle sampling method \cite{HARTLING201825}.\\\\\n")
        doc.write("The discrete structure of scattering angle in polyehtylene was also observed before in \\cite{TRAN201884} and can be seen both in ENDF\\textbackslash B-VII.1 and ENDF\\textbackslash B-VIII.0.")
        doc.write("\\begin{figure}[h!]\n")
        doc.write("\\centering\n")
        doc.write("\\includegraphics[height=11 cm]{"+path+"/auxiliary/micro.png}\n")
        doc.write("\\caption{Detailed view of the enlarged model of the microscopic benchmark with 50 primary particles.}\n")
        doc.write("\\end{figure}\n")

def createlibrarypage(doc,TSLibrary,librarydir):
    doc.write("\\section{"+librarydir.replace("_","\_")+"}\n")
    doc.write("The following results are calculated with data from "+TSLibrary.replace("_","\_")+" for both thermal region and higher energies in MCNP6 and Tripoli-4.\\\\\n")
    splitted_librarydir=librarydir.split("_")
    if ("G4NDL" in librarydir):
        doc.write("In Geant4, "+splitted_librarydir[0]+" is used as NeutronHP library with thermal scattering data from ")
    else:
        doc.write("In Geant4, "+splitted_librarydir[0]+" \cite{g4iaea3,g4iaea2,g4iaea1} is used as NeutronHP library with thermal scattering data from ")
    if len(splitted_librarydir)==4:
        if splitted_librarydir[3]=="HP":
            precision="high precision (tol=0.001, N$_\mu$=32), see \cite{THULLIEZ2022166187}."
        elif splitted_librarydir[3]=="LP":
            precision="low precision (tot=0.02, N$_\mu$=8), see \cite{THULLIEZ2022166187}."
        doc.write(splitted_librarydir[1]+" completed with missing libraries from "+splitted_librarydir[2]+" prepared with "+precision+"\\pagebreak\n")
    elif len(splitted_librarydir)==3:
        if splitted_librarydir[2]=="HP":
            precision="high precision (tol=0.001, N$_\mu$=32), see \cite{THULLIEZ2022166187}."
        elif splitted_librarydir[2]=="LP":
            precision="low precision (tol=0.02, N$_\mu$=8), see \cite{THULLIEZ2022166187}."
        doc.write(splitted_librarydir[1]+" prepared with "+precision+"\\pagebreak\n")
    elif len(splitted_librarydir)==2:
        doc.write(splitted_librarydir[1]+".\\pagebreak\n")

def createendingpage(doc,path,material_descriptions):
    doc.write("\\bibliographystyle{plain}\n")
    doc.write("\\bibliography{"+path+"/auxiliary/references}\n")
    doc.write("\\appendix\n")
    doc.write("\\lstset{breaklines=true,language=C++}\n")
    doc.write("\\chapter{Material descriptions}\n")
    with open(path+"/auxiliary/material_description.json") as jsonfile:
        descriptions=json.load(jsonfile)
    for material in material_descriptions:
        doc.write("\subsubsection{"+material.replace("_","\_")+"}\n")
        doc.write("\\begin{lstlisting}\n")
        doc.write(descriptions[material])
        doc.write("\\end{lstlisting}\n")
    doc.write("\\end{document}\n")
    doc.close()

def dostatistics(mac_or_mic,data,materialname,path,relative=3,absolut=0.02):
    key=list(data.keys())
    key.remove("G4")
    if mac_or_mic=="Macroscopic":
        text=r"The condition is: |relative difference|$\leq"+str(relative)+r"\cdot \sigma+"+str(absolut)+"$.\n\n"
        for i in key:
            text+="In comparison with "+i+" "
            difference_flux=data["G4"]["flux"]/data[i]["flux"]-1
            data["G4"]["diff_flux_"+i]=np.array(difference_flux)
            difference_flux_err=(np.sqrt(data["G4"]["flux_relativerr"]**2+
            data[i]["flux_relativerr"]**2)*(data["G4"]["flux"]/data[i]["flux"]))
            data["G4"]["diff_flux_err_"+i]=np.array(difference_flux_err)
            difference_flux[data["G4"]["flux"]==0]=0
            difference_flux[data[i]["flux"]==0]=0
            difference_flux_err[data["G4"]["flux"]==0]=0
            difference_flux_err[data[i]["flux"]==0]=0
            limit=relative*difference_flux_err+absolut
            booleanarray=(abs(difference_flux)<=limit)
            weights=1/(difference_flux_err**2)
            weights[np.isinf(weights)]=0
            meanrelativeerror=(np.sum(weights*abs(difference_flux))/np.sum(weights))*100
            sumrelativeerror=np.sum(abs(difference_flux))
            sumsquares=np.sum((data["G4"]["flux"]-data[i]["flux"])**2)
            if np.all(booleanarray):
                text+="the condition is met for all bins.\n\n"
                text+=r"The sum of squares (G4-"+i+")$^2$ is "+('%.4g' % sumsquares)+". "
                text+="The sum of relative difference |G4-"+i+"|/"+i+" is "+('%.4g' % sumrelativeerror)+".\\\\\n"
                text+="The weighted mean of relative difference |G4-"+i+"|/"+i+" in non-zero bins is "+('%.4g' % meanrelativeerror)+"\%."
            else:
                x=np.invert(booleanarray)
                text+="the condition is not met for "+str(np.sum(x))+" non-zero bins,\
                which is "+('%.4g' % (np.sum(x)/np.flatnonzero(data["G4"]["flux"]).size*100))+" \\% of all non-zero bins.\\\\\n"
                z = np.concatenate(([False], x, [False]))
                start = np.flatnonzero(~z[:-1] & z[1:])
                end = np.flatnonzero(z[:-1] & ~z[1:])
                indexes=np.column_stack((start, end))
                text+="These are the ranges (in eV), where the condition is not met:\\\\\n"
                for j in range(int(indexes.size/2)):
                    if j==(int(indexes.size/2)-1):
                        text+=(('%.6g' % (data["G4"]["x_low"][indexes[j][0]]*1e6))+"-"+
                        ('%.6g' % (data["G4"]["x_low"][indexes[j][1]]*1e6))+".\\\\\n")
                    else:
                        text+=(('%.6g' % (data["G4"]["x_low"][indexes[j][0]]*1e6))+"-"+
                        ('%.6g' % (data["G4"]["x_low"][indexes[j][1]]*1e6))+", ")
                text+=r"The sum of squares (G4-"+i+")$^2$ is "+('%.4g' % sumsquares)+". "
                text+="The sum of relative difference |G4-"+i+"|/"+i+" is "+('%.4g' % sumrelativeerror)+".\\\\\n"
                text+="The weighted mean of relative difference |G4-"+i+"|/"+i+" in non-zero bins is "+('%.4g' % meanrelativeerror)+"\%."
            if i!=key[-1]:
                text+="\n\n\\vspace{5mm}\n\n"
        with open(path+"/"+materialname+"_statistics_report"+".txt","w") as file:
            file.write(text)
        return text
    elif mac_or_mic=="Microscopic":
        text_cond=r"The condition is: |relative difference|$\leq"+str(relative)+r"\cdot \sigma+"+str(absolut)+"$.\n\n"
        text_e=""
        text_c=""
        for i in key:
            text_e+="In comparison with "+i+" for energy "
            difference_energy=data["G4"]["energy"]/data[i]["energy"]-1
            data["G4"]["diff_energy_"+i]=difference_energy
            difference_energy_err=(np.sqrt(data["G4"]["energy_relativerr"]**2+
            data[i]["energy_relativerr"]**2)*(data["G4"]["energy"]/data[i]["energy"]))
            data["G4"]["diff_energy_err_"+i]=difference_energy_err
            difference_energy[data["G4"]["energy"]==0]=0
            difference_energy[data[i]["energy"]==0]=0
            difference_energy_err[data["G4"]["energy"]==0]=0
            difference_energy_err[data[i]["energy"]==0]=0
            limit=relative*difference_energy_err+absolut
            booleanarray=(abs(difference_energy)<=limit)
            weights=1/(difference_energy_err**2)
            weights[np.isinf(weights)]=0
            meanrelativeerror=(np.sum(weights*abs(difference_energy))/np.sum(weights))*100
            sumrelativeerror=np.sum(abs(difference_energy))
            sumsquares=np.sum((data["G4"]["energy"]-data[i]["energy"])**2)
            if np.all(booleanarray):
                text_e+="the condition is met for all bins.\\\\\n"
                text_e+=r"The sum of squares (G4-"+i+")$^2$ is "+('%.4g' % sumsquares)+". "
                text_e+="The sum of relative difference |G4-"+i+"|/"+i+" is "+('%.4g' % sumrelativeerror)+".\\\\\n"
                text_e+="The weighted mean of relative difference |G4-"+i+"|/"+i+" in non-zero bins is "+('%.4g' % meanrelativeerror)+"\%."
            else:
                x=np.invert(booleanarray)
                falsebinssum=np.sum(x)
                text_e+="the condition is not met for "+str(np.sum(x))+" non-zero bins,\
                which is "+('%.4g' % (np.sum(x)/np.flatnonzero(data["G4"]["energy"]).size*100))+" \\% of all non-zero bins.\\\\\n"
                z = np.concatenate(([False], x, [False]))
                start = np.flatnonzero(~z[:-1] & z[1:])
                end = np.flatnonzero(z[:-1] & ~z[1:])
                indexes=np.column_stack((start, end))
                text_e+="These are the ranges (in eV), where the condition is not met:\\\\\n"
                for j in range(int(indexes.size/2)):
                    if j==(int(indexes.size/2)-1):
                        text_e+=(('%.6g' % (data["G4"]["x_e"][indexes[j][0]]*1e6))+"-"+
                        ('%.6g' % (data["G4"]["x_e"][indexes[j][1]]*1e6))+".\\\\\n")
                    else:
                        text_e+=(('%.6g' % (data["G4"]["x_e"][indexes[j][0]]*1e6))+"-"+
                        ('%.6g' % (data["G4"]["x_e"][indexes[j][1]]*1e6))+", ")
                text_e+=r"The sum of squares (G4-"+i+")$^2$ is "+('%.4g' % sumsquares)+". "
                text_e+="The sum of relative difference |G4-"+i+"|/"+i+" is "+('%.4g' % sumrelativeerror)+".\\\\\n"
                text_e+="The weighted mean of relative difference |G4-"+i+"|/"+i+" in non-zero bins is "+('%.4g' % meanrelativeerror)+"\%."
            if i!=key[-1]:
                text_e+="\n\n\\vspace{5mm}\n\n"
            text_c+="In comparison with "+i+" for angle "
            difference_costheta=data["G4"]["costheta"]/data[i]["costheta"]-1
            data["G4"]["diff_costheta_"+i]=difference_costheta
            difference_costheta_err=(np.sqrt(data["G4"]["costheta_relativerr"]**2+
            data[i]["costheta_relativerr"]**2)*(data["G4"]["costheta"]/data[i]["costheta"]))
            data["G4"]["diff_costheta_err_"+i]=difference_costheta_err
            difference_costheta[data["G4"]["costheta"]==0]=0
            difference_costheta[data[i]["costheta"]==0]=0
            difference_costheta_err[data["G4"]["costheta"]==0]=0
            difference_costheta_err[data[i]["costheta"]==0]=0
            limit=relative*difference_costheta_err+absolut
            booleanarray=(abs(difference_costheta)<=limit)
            weights=1/(difference_costheta_err**2)
            weights[np.isinf(weights)]=0
            meanrelativeerror=(np.sum(weights*abs(difference_costheta))/np.sum(weights))*100
            sumrelativeerror=np.sum(abs(difference_costheta))
            sumsquares=np.sum((data["G4"]["costheta"]-data[i]["costheta"])**2)
            if np.all(booleanarray):
                text_c+="the condition is met for all bins.\\\\\n"
                text_c+=r"The sum of squares (G4-"+i+")$^2$ is "+('%.4g' % sumsquares)+". "
                text_c+="The sum of relative difference |G4-"+i+"|/"+i+" is "+('%.4g' % sumrelativeerror)+".\\\\\n"
                text_c+="The weighted mean of relative difference |G4-"+i+"|/"+i+" in non-zero bins is "+('%.4g' % meanrelativeerror)+"\%."
            else:
                x=np.invert(booleanarray)
                falsebinssum=np.sum(x)
                text_c+="the condition is not met for "+str(np.sum(x))+" non-zero bins,\
                which is "+('%.4g' % (np.sum(x)/np.flatnonzero(data["G4"]["costheta"]).size*100))+" \\% of all non-zero bins.\\\\\n"
                z = np.concatenate(([False], x, [False]))
                start = np.flatnonzero(~z[:-1] & z[1:])
                end = np.flatnonzero(z[:-1] & ~z[1:])
                indexes=np.column_stack((start, end))
                text_c+=(r"These are the ranges (in $\mu$), where the condition is not met:\\"+"\n")
                for j in range(int(indexes.size/2)):
                    if j==(int(indexes.size/2)-1):
                        text_c+=(('%.6g' % (data["G4"]["x_c"][indexes[j][0]]))+"-"+
                        ('%.6g' % (data["G4"]["x_c"][indexes[j][1]]))+".\\\\\n")
                    else:
                        text_c+=(('%.6g' % (data["G4"]["x_c"][indexes[j][0]]))+"-"+
                        ('%.6g' % (data["G4"]["x_c"][indexes[j][1]]))+", ")
                text_c+=r"The sum of squares (G4-"+i+")$^2$ is "+('%.4g' % sumsquares)+". "
                text_c+="The sum of relative difference |G4-"+i+"|/"+i+" is "+('%.4g' % sumrelativeerror)+".\\\\\n"
                text_c+="The weighted mean of relative difference |G4-"+i+"|/"+i+" in non-zero bins is "+('%.4g' % meanrelativeerror)+"\%."
            if i!=key[-1]:
                text_c+="\n\n\\vspace{5mm}\n\n"
        text=text_cond+text_e+"\n\n"+text_c
        with open(path+"/"+materialname+"_statistics_report"+".txt","w") as file:
            file.write(text)
        return text_cond,text_e,text_c

def createpage(mac_or_mic,text,doc,path,material,data):
    key=list(data.keys())
    if mac_or_mic=="Macroscopic":
        doc.write("\subsection{"+material["material"].replace("_","\_")+" "+str(material["temperature"])+" K}\n")
        doc.write("The energy of initial neutrons is "+str(material["energy"])+" eV. \n")
        doc.write("The density of material is "+str(material["density"])+" g/cm3.\n")
        doc.write("\\begin{figure}[h!]\n")
        doc.write("\\centering\n")
        if len(key)==3:
            doc.write("\\includegraphics[width=0.95\\textwidth]{"+path+"/"+material["material"]+key[0]+key[1]+key[2]+"_flux.pdf}\n")
            doc.write("\\caption{Comparison of neutron flux calculated by Geant4, Tripoli{-}4 and MCNP6.2.}\n")
        else:
            if key[1]=="T4":
                program="Tripoli-4"
            if key[1]=="M6":
                program=="MCNP6.2"
            doc.write("\\includegraphics[width=0.95\\textwidth]{"+path+"/"+material["material"]+key[0]+key[1]+"_flux.pdf}\n")
            doc.write("\\caption{Comparison of neutron flux calculated by Geant4 and "+program+".}\n")
        doc.write("\\end{figure}\n\n")
        doc.write(text+"\\pagebreak\n\n")
    elif mac_or_mic=="Microscopic":
        doc.write("\subsection{"+material["material"].replace("_","\_")+" "+str(material["temperature"])+" K}\n")
        doc.write("The energy of initial neutrons is "+str(material["energy"])+" eV. \n")
        doc.write("The density of material is "+str(material["density"])+" g/cm3.\n")
        doc.write("\subsubsection{Energy}")
        doc.write("\\begin{figure}[h!]\n")
        if len(key)==3:
            doc.write("\\includegraphics[width=0.95\\textwidth]{"+path+"/"+material["material"]+key[0]+key[1]+key[2]+"_energy.pdf}\n")
            doc.write("\\caption{Comparison of scattered neutron energy calculated by Geant4, Tripoli-4 and MCNP6.2.}\n")
        else:
            if key[1]=="T4":
                program="Tripoli-4"
            if key[1]=="M6":
                program=="MCNP6.2"
            doc.write("\\includegraphics[width=0.95\\textwidth]{"+path+"/"+material["material"]+key[0]+key[1]+"_energy.pdf}\n")
            doc.write('\\caption{Comparison of scattered neutron energy calculated by Geant4 and '+program+".}\n")
        doc.write("\\end{figure}\n\n")
        doc.write(text[0])
        doc.write(text[1]+"\\pagebreak\n\n")
        doc.write("\subsubsection{Angle}")
        doc.write("\\begin{figure}[h!]\n")
        if len(key)==3:
            doc.write("\\includegraphics[width=0.95\\textwidth]{"+path+"/"+material["material"]+key[0]+key[1]+key[2]+"_angle.pdf}\n")
            doc.write(r"\caption{Comparison of $\cos \theta$ of scattered neutron calculated by Geant4, Tripoli-4 and MCNP6.2.}")
        else:
            if key[1]=="T4":
                program="Tripoli-4"
            if key[1]=="M6":
                program=="MCNP6.2"
            doc.write("\\includegraphics[width=0.95\\textwidth]{"+path+"/"+material["material"]+key[0]+key[1]+"_angle.pdf}\n")
            doc.write(r'\caption{Comparison of $\cos \theta$ of scattered neutron calculated by Geant4 and '+program+".}")
        doc.write("\n")
        doc.write("\\end{figure}\n\n")
        doc.write(text[0])
        doc.write(text[2]+"\\pagebreak\n\n")


def createtitlepageDBRC(path,version):
    doc=open(path+"/Geant4."+version+"DBRCBenchmark.tex", "w")
    doc.write("\\documentclass{report}\n")
    doc.write("\\usepackage[T1]{fontenc}\n")
    doc.write("\\usepackage[utf8]{inputenc}\n")
    doc.write("\\usepackage{lmodern}\n")
    doc.write("\\usepackage{textcomp}\n")
    doc.write("\\usepackage{listings}\n")
    doc.write("\\usepackage{geometry}\n")
    doc.write("\\geometry{tmargin=2cm,lmargin=1.5cm,rmargin=1.5cm,bmargin=2cm}\n")
    doc.write("\\usepackage{hyperref}\n")
    doc.write("\\usepackage{graphicx}\n")
    doc.write("\\usepackage{subcaption}\n")
    doc.write("\\usepackage{amsmath}\n")
    doc.write("\\usepackage{etoolbox}\n")
    doc.write("\\patchcmd{\\abstract}{\\titlepage}{}{}{}\n")
    doc.write("\\patchcmd{\\endabstract}{\\endtitlepage}{}{}\n")
    doc.write("\n")
    doc.write("\\title{Geant4."+version+" DBRC Benchmark}\n")
    doc.write(r"""\author{Marek Zmeskal$^{a,b,*}$, Loic Thulliez$^{c}$, Eric Dumonteil$^{c}$ \\
    \\
    $^a$\textit{Dept. of Nuclear Reactors, Faculty of Nuclear Sciences and Physical Engineering,}\\
    $^b$\textit{Research Centre Rez, Hlavni 130, 250 68 Husinec-Rez, Czech Republic}\\
    \textit{Czech Technical University in Prague, V Holesovickach 2, 180 00 Prague, Czech Republic}\\
    $^c$\textit{IRFU, CEA, Université Paris{-}Saclay, 911 91 Gif{-}sur{-}Yvette, France}\\
    \\
    $^*$\textit{email: zmeskma1@fjfi.cvut.cz} }"""+"\n")
    doc.write(r"""\date{Report created on\\
    \today \\
    with the version of code and database from \\
    February 26, 2022}"""+"\n")
    doc.write("\\begin{document}\n")
    #doc.write("\\normalsize\n")
    doc.write("\\maketitle\n")
    doc.write("\\begin{titlepage}\n")
    doc.write("\\begin{abstract}\n")
    doc.write("This report shows comparison of Geant4 scattering on resonant nuclei with the use of SVT and DBRC method. It is compared with the reference neutron transport code Tripoli-4 (version 11). This report can be used to ensure proper DBRC treatment in all future versions of Geant4.\n")
    doc.write("\\end{abstract}\n")
    doc.write("\\renewcommand{\\abstractname}{Acknowledgements}\n")
    doc.write("\\begin{abstract}\n")
    doc.write("This work could be done thanks to the IAEA´s technical cooperation programme, which supported a fellowship of Marek Zmeskal at IRFU, CEA in the framework of national project CZR0011 - Strengthening Human Resources Capacity in Nuclear Science and Technology. The authors wish to sincerely thank Alberto Ribon, head of the Geant4 Hadronic group, for fruitful discussions. Tripoli-4 is a registered trademark of CEA. \n")
    doc.write("\\end{abstract}\n")
    doc.write("\\end{titlepage}\n")
    doc.write("\\tableofcontents\n")
    doc.write("\n")
    return doc

def createheaderpageDBRC(doc,path,exponent):
    doc.write("\\section{Description}\n")
    doc.write("This test uses Microscopic benchmark, which is aimed at the precise description of the spectral characteristics of individual scatterings.\\\\\n")
    doc.write(r"Microscopic benchmark consists of a thin cylinder with radius of 1 $\mu$m and length 2 m.\\"+"\n")
    doc.write("The source of neutrons is monoenergetic point\
     source with direction parallel to axis z with direction in +z and placed at the -z end of the cylinder.\\\\\n")
    doc.write("The simulation was run with 1E8 particles in Tripoli-4 and with 1E"+str(exponent)+" particles in Geant4.\\\\\n")
    doc.write("The scored and plotted quantities are energy of the scattered neutron after the first collision. The result is normalized to 1E8 particles.\\\\\n")
    doc.write("\\begin{figure}[h!]\n")
    doc.write("\\centering\n")
    doc.write("\\includegraphics[height=11 cm]{"+path+"/auxiliary/micro.png}\n")
    doc.write("\\caption{Detailed view of the enlarged model of the microscopic benchmark with 50 primary particles.}\n")
    doc.write("\\end{figure}\n")

def createlibrarypageDBRC(doc,library,libraryHP):
    doc.write("\\section{"+library.replace("_","\_")+"\_"+libraryHP+"}\n")
    doc.write("The following results are calculated with data from "+library.replace("_","\_")+" in Tripoli-4.\\\\\n")
    if ("G4NDL" in libraryHP):
        doc.write("In Geant4, "+libraryHP+" is used as NeutronHP library.\n")
    else:
        doc.write("In Geant4, "+libraryHP+" \cite{g4iaea3,g4iaea2,g4iaea1} is used as NeutronHP library.\n")
    doc.write("\\pagebreak\n")

def createendingpageDBRC(doc,path):
    doc.write("\\bibliographystyle{plain}\n")
    doc.write("\\bibliography{"+path+"/auxiliary/references}\n")
    doc.write("\\end{document}\n")
    doc.close()

def createenergypageDBRC(doc,energy,path,material):
    #doc.write("\subsubsection{"str(energy)+" eV}\n")
    doc.write("""\\begin{figure}[h!]
     \centering""")
    for temperature in material["temperatures"]:
        doc.write("""
          \\begin{subfigure}[b]{0.49\\textwidth}
              \centering
              \includegraphics[width=\\textwidth]{"""+path+"/Energy_woutDBRC_"+str(energy)+"_"+str(temperature)+".pdf"+"""}
              \caption{"""+str(energy)+""" eV - """+str(temperature)+""" K - SVT}
              \label{fig:}
          \end{subfigure}
          \hfill
          \\begin{subfigure}[b]{0.49\\textwidth}
              \centering
              \includegraphics[width=\\textwidth]{"""+path+"/Energy_wDBRC_"+str(energy)+"_"+str(temperature)+".pdf"+"""}
              \caption{"""+str(energy)+""" eV - """+str(temperature)+""" K - DBRC}
              \label{fig:}
          \end{subfigure}
         """)
    doc.write("""\caption{Outgoing neutron energy distributions of scattered neutrons for """+str(energy)+""" eV neutrons on """+material["material"]+""" at different temperatures computed with SVT (left column) and DBRC (right column) computed with Geant4 and Tripoli-4. The bottom part presents the relative difference G4/T4-1 (green solid line) with three times the standard statistical error of this difference (dashed line).}
        \label{fig:comparison}
\end{figure}\n""")
    doc.write("\\pagebreak\n")

def createtablepageDBRC(doc,material,data):
    doc.write("""
    \\begin{table}[h!]
\centering
\caption{Average energy of the scattered neutrons in eV for different primary energies and temperatures on """+material["material"]+""" with SVT and DBRC methods computed with Geant4 and Tripoli-4. The relative difference (rel. diff.) between the two codes are in~$10^{-3}$ \%.}
\\begin{tabular}{rr|cc|cc|cc}
\multicolumn{1}{c}{E}    & \multicolumn{1}{c|}{T}   & \multicolumn{2}{c|}{Geant4} & \multicolumn{2}{c|}{Tripoli-4} & \multicolumn{2}{c}{rel. diff.} \\\\
\multicolumn{1}{c}{(eV)} & \multicolumn{1}{c|}{(K)} & SVT          & DBRC        & SVT            & DBRC          & SVT            & DBRC          \\\\ \hline
""")
    i_e=-1
    n_t=len(material["temperatures"])
    for energy in material["energies"]:
        i_e+=1
        doc.write(str(energy))
        i_t=-1
        for temperature in material["temperatures"]:
            i_t+=1
            doc.write(" & "+str(temperature)+" & "+"{:.3f}".format(data[2*(i_e*n_t+i_t),4])+" & "+"{:.3f}".format(data[2*(i_e*n_t+i_t),10])+" & "+"{:.3f}".format(data[2*(i_e*n_t+i_t)+1,4])+" & "+"{:.3f}".format(data[2*(i_e*n_t+i_t)+1,10])+" & "+"{:.2f}".format((data[2*(i_e*n_t+i_t),4]/data[2*(i_e*n_t+i_t)+1,4]-1)*100000)+" & "+"{:.2f}".format((data[2*(i_e*n_t+i_t),10]/data[2*(i_e*n_t+i_t)+1,10]-1)*100000)+" \\\\" +" \n")
        if energy==material["energies"][-1]:
            doc.write("""\end{tabular}
            \end{table}
            """)
        else:
            doc.write("\hline\n")
    doc.write("""
    \\begin{table}[h!]
\centering
\caption{Up-scattering probability in \% for different primary energies and temperatures on """+material["material"]+""" computed with SVT and DBRC methods with Geant4 and Tripoli-4. The relative difference (rel. diff.) between the two codes are in \%.}
\label{tab:upscattering}
\\begin{tabular}{rr|cc|cc|cc}
\multicolumn{1}{c}{E}    & \multicolumn{1}{c|}{T}   & \multicolumn{2}{c|}{Geant4} & \multicolumn{2}{c|}{Tripoli-4} & \multicolumn{2}{c}{rel. diff.} \\\\
\multicolumn{1}{c}{(eV)} & \multicolumn{1}{c|}{(K)} & SVT          & DBRC        & SVT            & DBRC          & SVT            & DBRC          \\\\ \hline
""")
    i_e=-1
    n_t=len(material["temperatures"])
    for energy in material["energies"]:
        i_e+=1
        doc.write(str(energy))
        i_t=-1
        for temperature in material["temperatures"]:
            i_t+=1
            doc.write(" & "+str(temperature)+" & "+"{:.3f}".format(data[2*(i_e*n_t+i_t),2])+" & "+"{:.3f}".format(data[2*(i_e*n_t+i_t),8])+" & "+"{:.3f}".format(data[2*(i_e*n_t+i_t)+1,2])+" & "+"{:.3f}".format(data[2*(i_e*n_t+i_t)+1,8])+" & "+"{:.2f}".format((data[2*(i_e*n_t+i_t),2]/data[2*(i_e*n_t+i_t)+1,2]-1)*100)+" & "+"{:.2f}".format((data[2*(i_e*n_t+i_t),8]/data[2*(i_e*n_t+i_t)+1,8]-1)*100)+" \\\\" +" \n")
        if energy==material["energies"][-1]:
            doc.write("""\end{tabular}
            \end{table}
            """)
        else:
            doc.write("\hline\n")
    doc.write("\\pagebreak\n\n")
