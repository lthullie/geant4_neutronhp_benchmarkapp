import matplotlib.pyplot as plt
import matplotlib.ticker
import numpy as np

def testandgetx(mac_or_mic,data,directory):
    keys=list(data.keys())
    keys.remove("G4")
    if mac_or_mic=="Macroscopic":
        for key in keys:
            if (not np.allclose(data["G4"]["x_low"],data[key]["x_low"],rtol=1e-7,atol=0)):
                callout=("The energy groups in this directory "+directory+" for Geant4"+
                " are not equal with "+key+".")
                return [True,callout]
        xcenter=np.zeros((data["G4"]["x_low"].size-1))
        for i in range(data["G4"]["x_low"].size-1):
            xcenter[i]=(data["G4"]["x_low"][i]+data["G4"]["x_low"][i+1])/2*1e6
        return [False,xcenter]
    if mac_or_mic=="Microscopic":
        for key in keys:
            if (not np.allclose(data["G4"]["x_e"],data[key]["x_e"],rtol=1e-7,atol=0)):
                callout=("The energy groups in this directory "+directory+" for Geant4"+
                " are not equal with "+key+".")
                return [True,callout]
            if (not np.allclose(data["G4"]["x_c"],data[key]["x_c"],rtol=1e-7,atol=1e-15)):
                callout=("The cosine groups in this directory "+directory+" for Geant4"+
                " are not equal with "+key+".")
                return [True,callout]
        xecenter=np.zeros((data["G4"]["x_e"].size-1))
        for i in range(data["G4"]["x_e"].size-1):
            xecenter[i]=(data["G4"]["x_e"][i]+data["G4"]["x_e"][i+1])/2*1e6
        xccenter=np.zeros((data["G4"]["x_c"].size-1))
        for i in range(data["G4"]["x_c"].size-1):
            xccenter[i]=(data["G4"]["x_c"][i]+data["G4"]["x_c"][i+1])/2
        return [False,xecenter,xccenter]


def plotmacro(xcenter,data,path,material):
    keys=list(data.keys())
    keys.remove("G4")
    for key in keys:
        fig, (ax1, ax2) = plt.subplots(2,1,height_ratios=[5,3.5],sharex=True,figsize=(7.2, 5.32))

        #set axis limits
        if (data[key]["flux"][50]>2e-9) or (data["G4"]["flux"][50]>2e-9):
            myxmin=1e-5
        else:
            myxmin=1e-4
        ax1.set_xlim(myxmin,20)
        ymax=1.5*np.amax(data["G4"]["flux"])
        ax1.set_ylim(ymin=1e-10,ymax=ymax)
        ax2.set_ylim(-0.2,0.2)

        #plotting
        lw=1.3
        ax1.plot(xcenter,data["G4"]["flux"],'b-',drawstyle='steps-mid',lw=lw,label="G4")
        ax1.plot(xcenter,data[key]["flux"],'g-',drawstyle='steps-mid',lw=lw,label=key)
        ax2.plot(xcenter,3*data["G4"]["diff_flux_err_"+key],'k--',lw=0.5,drawstyle='steps-mid',label='3 sigma')
        ax2.plot(xcenter,-3*data["G4"]["diff_flux_err_"+key],'k--',lw=0.5,drawstyle='steps-mid')
        ax2.plot([-10,100],[0,0],'k:',lw=0.5)
        ax2.plot(xcenter,data["G4"]["diff_flux_"+key],'r-',label="G4"+"/"+key+"-1")

        #axis labels and lin/log
        ax2.set_xlabel('neutron energy (eV)',fontsize=11)
        ax1.set_ylabel('neutron flux (cm$^{-2}$)',fontsize=11)
        ax2.set_ylabel("relative difference",fontsize=11)
        ax1.set_xscale("log")
        ax2.set_xscale("log")
        ax1.set_yscale("log")
        ax2.set_yscale("linear")

        #ticks
        ax1.minorticks_on()
        ax2.minorticks_on()
        ax12 = ax1.secondary_yaxis("right")
        ax13 = ax1.secondary_xaxis("top")
        ax22 = ax2.secondary_yaxis("right")
        ax23 = ax2.secondary_xaxis("top")
        ax22.minorticks_on()
        majorticklength=6
        minorticklenght=3
        ax1.tick_params(labelsize=11,length=majorticklength, width=0.9)
        ax1.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax2.tick_params(labelsize=11,length=majorticklength, width=0.9)
        ax2.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax12.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax13.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax22.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax23.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax12.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax13.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)
        ax22.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax23.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)

        #legend and save
        ax1.legend(loc='best',fontsize=11,labelcolor='k',ncol=1)
        ax2.legend(loc='best',fontsize=11,labelcolor='k',ncol=1)
        plt.savefig(path+"/"+material+"G4"+key+"_flux.pdf",dpi=500, bbox_inches = 'tight')
        plt.close()

    if len(keys)==2:
        fig, (ax1, ax2) = plt.subplots(2,1,height_ratios=[5,3.5],sharex=True,figsize=(7.2, 5.32))

        #set axis limits
        if (data[key]["flux"][50]>2e-9) or (data["G4"]["flux"][50]>2e-9):
            myxmin=1e-5
        else:
            myxmin=1e-4
        ax1.set_xlim(myxmin,20)
        ymax=1.5*np.amax(data["G4"]["flux"])
        ax1.set_ylim(ymin=1e-10,ymax=ymax)
        ax2.set_ylim(-0.2,0.2)

        #plotting
        lw=1.3
        ax1.plot(xcenter,data["G4"]["flux"],'b-',drawstyle='steps-mid',lw=lw,label="G4")
        ax1.plot(xcenter,data[keys[0]]["flux"],'g-',drawstyle='steps-mid',lw=lw,label=keys[0])
        ax1.plot(xcenter,data[keys[1]]["flux"],'m-',drawstyle='steps-mid',lw=lw,label=keys[1])
        ax2.plot(xcenter,3*data["G4"]["diff_flux_err_"+keys[0]],'g--',lw=0.5,drawstyle='steps-mid')
        ax2.plot(xcenter,-3*data["G4"]["diff_flux_err_"+keys[0]],'g--',lw=0.5,drawstyle='steps-mid')
        ax2.plot(xcenter,3*data["G4"]["diff_flux_err_"+keys[1]],'m--',lw=0.5,drawstyle='steps-mid')
        ax2.plot(xcenter,-3*data["G4"]["diff_flux_err_"+keys[1]],'m--',lw=0.5,drawstyle='steps-mid')
        ax2.plot([-10],[0],'k--',lw=0.5,label='3 sigma')
        ax2.plot([-10,100],[0,0],'k:',lw=0.5)
        ax2.plot(xcenter,data["G4"]["diff_flux_"+keys[0]],'g-',label="G4"+"/"+keys[0]+"-1")
        ax2.plot(xcenter,data["G4"]["diff_flux_"+keys[1]],'m-',label="G4"+"/"+keys[1]+"-1")

        #axis labels and lin/log10
        ax2.set_xlabel('neutron energy (eV)',fontsize=11)
        ax1.set_ylabel('neutron flux (cm$^{-2}$)',fontsize=11)
        ax2.set_ylabel("relative difference",fontsize=11)
        ax1.set_xscale("log")
        ax2.set_xscale("log") #linear, log
        ax1.set_yscale("log") #linear, log
        ax2.set_yscale("linear") #linear, log

        #ticks
        ax1.minorticks_on()
        ax2.minorticks_on()
        ax12 = ax1.secondary_yaxis("right")
        ax13 = ax1.secondary_xaxis("top")
        ax22 = ax2.secondary_yaxis("right")
        ax23 = ax2.secondary_xaxis("top")
        ax22.minorticks_on()
        majorticklength=6
        minorticklenght=3
        ax1.tick_params(labelsize=11,length=majorticklength, width=0.9)
        ax1.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax2.tick_params(labelsize=11,length=majorticklength, width=0.9)
        ax2.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax12.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax13.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax22.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax23.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax12.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax13.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)
        ax22.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax23.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)

        #legend and save
        ax1.legend(loc='best',fontsize=11,labelcolor='k',ncol=1)  # Add a legend.
        ax2.legend(loc='best',fontsize=11,labelcolor='k',ncol=1)  # Add a legend.
        plt.savefig(path+"/"+material+"G4"+keys[0]+keys[1]+"_flux.pdf",dpi=500, bbox_inches = 'tight')
        plt.close()

def plotmicro(xecenter,xccenter,data,path,material):
    keys=list(data.keys())
    keys.remove("G4")
    for key in keys:
        #energy graph
        fig, (ax1, ax2) = plt.subplots(2,1,height_ratios=[5,3.5],sharex=True,figsize=(7.2, 5.32))

        #set axis limits
        if (data[key]["energy"][50]>100) or (data["G4"]["energy"][50]>100):
            myxmin=1e-5
        else:
            myxmin=1e-4
        ax1.set_xlim(myxmin,1)
        ymax=1.5*np.amax(data["G4"]["energy"])
        ax1.set_ylim(ymin=1,ymax=ymax)
        ax2.set_ylim(-0.5,0.5)

        #plotting
        lw=1.3
        ax1.plot(xecenter,data["G4"]["energy"],'b-',drawstyle='steps-mid',lw=lw,label="G4")
        ax1.plot(xecenter,data[key]["energy"],'g-',drawstyle='steps-mid',lw=lw,label=key)
        ax2.plot(xecenter,3*data["G4"]["diff_energy_err_"+key],'k--',lw=0.5,drawstyle='steps-mid',label='3 sigma')
        ax2.plot(xecenter,-3*data["G4"]["diff_energy_err_"+key],'k--',lw=0.5,drawstyle='steps-mid')
        ax2.plot([-10,100],[0,0],'k:',lw=0.5)
        ax2.plot(xecenter,data["G4"]["diff_energy_"+key],'r-',label="G4"+"/"+key+"-1")

        #axis labels and lin/log
        ax2.set_xlabel('neutron energy (eV)',fontsize=11)
        ax1.set_ylabel('counts',fontsize=11)
        ax2.set_ylabel("relative difference",fontsize=11)
        ax1.set_xscale("log")
        ax2.set_xscale("log") #linear, log
        ax1.set_yscale("log") #linear, log
        ax2.set_yscale("linear") #linear, log

        #ticks
        ax1.minorticks_on()
        ax2.minorticks_on()
        ax12 = ax1.secondary_yaxis("right")
        ax13 = ax1.secondary_xaxis("top")
        ax22 = ax2.secondary_yaxis("right")
        ax23 = ax2.secondary_xaxis("top")
        y_major = matplotlib.ticker.LogLocator(base = 10.0, numticks = 5)
        ax1.yaxis.set_major_locator(y_major)
        ax12.yaxis.set_major_locator(y_major)
        y_minor = matplotlib.ticker.LogLocator(base = 10.0, subs = np.arange(1.0, 10.0) * 0.1, numticks = 10)
        ax1.yaxis.set_minor_locator(y_minor)
        ax1.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
        ax12.yaxis.set_minor_locator(y_minor)
        ax12.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
        ax22.minorticks_on()
        majorticklength=6
        minorticklenght=3
        ax1.tick_params(labelsize=11,length=majorticklength, width=0.9)
        ax1.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax2.tick_params(labelsize=11,length=majorticklength, width=0.9)
        ax2.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax12.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax13.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax22.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax23.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax12.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax13.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)
        ax22.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax23.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)

        #legend and save
        ax1.legend(loc='best',fontsize=11,labelcolor='k',ncol=1)
        ax2.legend(loc='best',fontsize=11,labelcolor='k',ncol=1)
        plt.savefig(path+"/"+material+"G4"+key+"_energy.pdf",dpi=500, bbox_inches = 'tight')
        plt.close()


        #cosine graph
        fig, (ax1, ax2) = plt.subplots(2,1,height_ratios=[5,3.5],sharex=True,figsize=(7.2, 5.32))

        #set axis limits
        ax1.set_xlim(-1,1)
        ax2.set_ylim(-0.2,0.2)

        #plotting
        lw=1.3
        ax1.plot(xccenter,data["G4"]["costheta"],'b-',drawstyle='steps-mid',lw=lw,label="G4")
        ax1.plot(xccenter,data[key]["costheta"],'g-',drawstyle='steps-mid',lw=lw,label=key)
        ax2.plot(xccenter,3*data["G4"]["diff_costheta_err_"+key],'k--',lw=0.5,drawstyle='steps-mid',label='3 sigma')
        ax2.plot(xccenter,-3*data["G4"]["diff_costheta_err_"+key],'k--',lw=0.5,drawstyle='steps-mid')
        ax2.plot([-10,100],[0,0],'k:',lw=0.5)
        ax2.plot(xccenter,data["G4"]["diff_costheta_"+key],'r-',label="G4"+"/"+key+"-1")

        #axis labels and lin/log
        ax2.set_xlabel(r'$\cos\theta$',fontsize=11)
        ax1.set_ylabel('counts',fontsize=11)
        ax2.set_ylabel("relative difference",fontsize=11)
        ax1.set_xscale("linear")
        ax2.set_xscale("linear") #linear, log
        ax1.set_yscale("log") #linear, log
        ax2.set_yscale("linear") #linear, log

        #ticks
        ax1.minorticks_on()
        ax2.minorticks_on()
        ax12 = ax1.secondary_yaxis("right")
        ax13 = ax1.secondary_xaxis("top")
        ax22 = ax2.secondary_yaxis("right")
        ax23 = ax2.secondary_xaxis("top")
        ax22.minorticks_on()
        ax13.minorticks_on()
        ax23.minorticks_on()
        majorticklength=6
        minorticklenght=3
        ax1.tick_params(labelsize=11,length=majorticklength, width=0.9)
        ax1.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax2.tick_params(labelsize=11,length=majorticklength, width=0.9)
        ax2.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax12.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax13.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax22.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax23.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax12.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax13.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)
        ax22.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax23.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)

        #legend and save
        ax1.legend(loc='best',fontsize=11,labelcolor='k',ncol=1)  # Add a legend.
        ax2.legend(loc='best',fontsize=11,labelcolor='k',ncol=1)  # Add a legend.
        plt.savefig(path+"/"+material+"G4"+key+"_angle.pdf",dpi=500, bbox_inches = 'tight')
        plt.close()

    if len(keys)==2:
        #energy graph
        fig, (ax1, ax2) = plt.subplots(2,1,height_ratios=[5,3.5],sharex=True,figsize=(7.2, 5.32))

        #set axis limits
        if (data[key]["energy"][50]>100) or (data["G4"]["energy"][50]>100):
            myxmin=1e-5
        else:
            myxmin=1e-4
        ax1.set_xlim(myxmin,1)
        ymax=1.5*np.amax(data["G4"]["energy"])
        ax1.set_ylim(ymin=1,ymax=ymax)
        ax2.set_ylim(-0.5,0.5)

        #plotting
        lw=1.3
        ax1.plot(xecenter,data["G4"]["energy"],'b-',drawstyle='steps-mid',lw=lw,label="G4")
        ax1.plot(xecenter,data[keys[0]]["energy"],'g-',drawstyle='steps-mid',lw=lw,label=keys[0])
        ax1.plot(xecenter,data[keys[1]]["energy"],'m-',drawstyle='steps-mid',lw=lw,label=keys[1])
        ax2.plot(xecenter,3*data["G4"]["diff_energy_err_"+keys[0]],'g--',lw=0.5,drawstyle='steps-mid')
        ax2.plot(xecenter,-3*data["G4"]["diff_energy_err_"+keys[0]],'g--',lw=0.5,drawstyle='steps-mid')
        ax2.plot(xecenter,3*data["G4"]["diff_energy_err_"+keys[1]],'m--',lw=0.5,drawstyle='steps-mid')
        ax2.plot(xecenter,-3*data["G4"]["diff_energy_err_"+keys[1]],'m--',lw=0.5,drawstyle='steps-mid')
        ax2.plot([-10],[0],'k--',lw=0.5,label='3 sigma')
        ax2.plot([-10,100],[0,0],'k:',lw=0.5)
        ax2.plot(xecenter,data["G4"]["diff_energy_"+keys[0]],'g-',label="G4"+"/"+keys[0]+"-1")
        ax2.plot(xecenter,data["G4"]["diff_energy_"+keys[1]],'m-',label="G4"+"/"+keys[1]+"-1")

        #axis labels and lin/log
        ax2.set_xlabel('neutron energy (eV)',fontsize=11)
        ax1.set_ylabel('counts',fontsize=11)
        ax2.set_ylabel("relative difference",fontsize=11)
        ax1.set_xscale("log")
        ax2.set_xscale("log") #linear, log
        ax1.set_yscale("log") #linear, log
        ax2.set_yscale("linear") #linear, log

        #ticks
        ax1.minorticks_on()
        ax2.minorticks_on()
        ax12 = ax1.secondary_yaxis("right")
        ax13 = ax1.secondary_xaxis("top")
        ax22 = ax2.secondary_yaxis("right")
        ax23 = ax2.secondary_xaxis("top")
        y_major = matplotlib.ticker.LogLocator(base = 10.0, numticks = 5)
        ax1.yaxis.set_major_locator(y_major)
        ax12.yaxis.set_major_locator(y_major)
        y_minor = matplotlib.ticker.LogLocator(base = 10.0, subs = np.arange(1.0, 10.0) * 0.1, numticks = 10)
        ax1.yaxis.set_minor_locator(y_minor)
        ax1.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
        ax12.yaxis.set_minor_locator(y_minor)
        ax12.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
        ax22.minorticks_on()
        majorticklength=6
        minorticklenght=3
        ax1.tick_params(labelsize=11,length=majorticklength, width=0.9)
        ax1.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax2.tick_params(labelsize=11,length=majorticklength, width=0.9)
        ax2.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax12.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax13.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax22.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax23.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax12.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax13.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)
        ax22.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax23.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)

        #legend and save
        ax1.legend(loc='best',fontsize=11,labelcolor='k',ncol=1)  # Add a legend.
        ax2.legend(loc='best',fontsize=11,labelcolor='k',ncol=1)  # Add a legend.
        plt.savefig(path+"/"+material+"G4"+keys[0]+keys[1]+"_energy.pdf",dpi=500, bbox_inches = 'tight')
        plt.close()


        #cosine graph
        fig, (ax1, ax2) = plt.subplots(2,1,height_ratios=[5,3.5],sharex=True,figsize=(7.2, 5.32))

        #set axis limits
        ax1.set_xlim(-1,1)
        ax2.set_ylim(-0.2,0.2)

        #plotting
        lw=1.3
        ax1.plot(xccenter,data["G4"]["costheta"],'b-',drawstyle='steps-mid',lw=lw,label="G4")
        ax1.plot(xccenter,data[keys[0]]["costheta"],'g-',drawstyle='steps-mid',lw=lw,label=keys[0])
        ax1.plot(xccenter,data[keys[1]]["costheta"],'m-',drawstyle='steps-mid',lw=lw,label=keys[1])
        ax2.plot(xccenter,3*data["G4"]["diff_costheta_err_"+keys[0]],'g--',lw=0.5,drawstyle='steps-mid')
        ax2.plot(xccenter,-3*data["G4"]["diff_costheta_err_"+keys[0]],'g--',lw=0.5,drawstyle='steps-mid')
        ax2.plot(xccenter,3*data["G4"]["diff_costheta_err_"+keys[1]],'m--',lw=0.5,drawstyle='steps-mid')
        ax2.plot(xccenter,-3*data["G4"]["diff_costheta_err_"+keys[1]],'m--',lw=0.5,drawstyle='steps-mid')
        ax2.plot([-10],[0],'k--',lw=0.5,label='3 sigma')
        ax2.plot([-10,100],[0,0],'k:',lw=0.5)
        ax2.plot(xccenter,data["G4"]["diff_costheta_"+keys[0]],'g-',label="G4"+"/"+keys[0]+"-1")
        ax2.plot(xccenter,data["G4"]["diff_costheta_"+keys[1]],'m-',label="G4"+"/"+keys[1]+"-1")

        #axis labels and lin/log
        ax2.set_xlabel(r'$\cos\theta$',fontsize=11)
        ax1.set_ylabel('counts',fontsize=11)
        ax2.set_ylabel("relative difference",fontsize=11)
        ax1.set_xscale("linear")
        ax2.set_xscale("linear") #linear, log
        ax1.set_yscale("log") #linear, log
        ax2.set_yscale("linear") #linear, log

        #ticks
        ax1.minorticks_on()
        ax2.minorticks_on()
        ax12 = ax1.secondary_yaxis("right")
        ax13 = ax1.secondary_xaxis("top")
        ax22 = ax2.secondary_yaxis("right")
        ax23 = ax2.secondary_xaxis("top")
        ax22.minorticks_on()
        ax13.minorticks_on()
        ax23.minorticks_on()
        majorticklength=6
        minorticklenght=3
        ax1.tick_params(labelsize=11,length=majorticklength, width=0.9)
        ax1.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax2.tick_params(labelsize=11,length=majorticklength, width=0.9)
        ax2.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax12.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax13.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax22.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax23.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax12.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax13.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)
        ax22.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax23.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)

        #legend and save
        ax1.legend(loc='best',fontsize=11,labelcolor='k',ncol=1)  # Add a legend.
        ax2.legend(loc='best',fontsize=11,labelcolor='k',ncol=1)  # Add a legend.
        plt.savefig(path+"/"+material+"G4"+keys[0]+keys[1]+"_angle.pdf",dpi=500, bbox_inches = 'tight')
        plt.close()


def plotmicroDBRC(xecenter,data,energy,temperature,w,path):
    yticksbool=False
    vertx=energy
    if(energy==6.52):
        if w=="w":
            if temperature==300:
                xmin=6.2
                xmax=6.8
            if temperature==600:
                xmin=6.1
                xmax=6.9
                yticksbool=True
                yticks=[2.5e4,5e4,7.5e4,10e4,12.5e4,15e4]
            if temperature==1000:
                xmin=6.1
                xmax=7.0
                yticksbool=True
                yticks=[5e4,10e4,15e4,20e4]
            xmin=6.2
            xmax=6.9
        if w=="wout":
            if temperature==300:
                xmin=6.2
                xmax=6.8
            if temperature==600:
                xmin=6.1
                xmax=6.8
                yticksbool=True
                yticks=[2.5e4,5e4,7.5e4,10e4,12.5e4,15e4]
            if temperature==1000:
                xmin=6.0
                xmax=6.9
                yticksbool=True
                yticks=[5e4,10e4,15e4,20e4]
            xmin=6.0
            xmax=6.8
        xmin=6.0
        xmax=6.9
    if(energy==20.2):
        if w=="w":
            if temperature==300:
                xmin=19.4
                xmax=20.4
            if temperature==600:
                xmin=19.3
                xmax=20.6
                yticksbool=True
                yticks=[1e4,2e4,3e4,4e4]
            if temperature==1000:
                xmin=6.1
                xmax=7.0
            xmin=19.2
            xmax=20.8
        if w=="wout":
            if temperature==300:
                xmin=19.5
                xmax=20.4
            if temperature==600:
                xmin=6.1
                xmax=6.8
                yticksbool=True
                yticks=[1e4,2e4,3e4,4e4]
            if temperature==1000:
                xmin=6.0
                xmax=6.9
            xmin=19.3
            xmax=20.6
        xmin=19.2
        xmax=20.8
    if(energy==36.25):
        if w=="w":
            if temperature==300:
                xmin=6.2
                xmax=6.8
            if temperature==600:
                xmin=6.1
                xmax=6.9
            if temperature==1000:
                xmin=6.1
                xmax=7.0
            xmin=35
            xmax=37
        if w=="wout":
            if temperature==300:
                xmin=6.2
                xmax=6.8
            if temperature==600:
                xmin=6.1
                xmax=6.8
            if temperature==1000:
                xmin=6.0
                xmax=6.9
            xmin=34.7
            xmax=36.9
        xmin=34.7
        xmax=37
    codes=list(data.keys())
    key=list(codes)
    codes.remove("G4")
    for code in codes:
        #calculating differences
        difference_energy=data["G4"][w]["energy"]/data[code][w]["energy"]-1

        fig, (ax1, ax2) = plt.subplots(2,1,height_ratios=[2,1],sharex=True,figsize=(7.2, 5.32))
        fig.tight_layout()

        #set axis limits
        ax1.set_xlim(xmin,xmax)
        ymax1=1.05*np.amax(data["G4"]["w"]["energy"])
        ymax2=1.05*np.amax(data["G4"]["wout"]["energy"])
        ymax=np.maximum(ymax1, ymax2)
        ax1.set_ylim(0,ymax)
        ax2.set_ylim(-0.2,0.2)
        lw=1.5

        ax1.plot(xecenter,data["G4"][w]["energy"],'b-',drawstyle='steps-mid',lw=2*lw,label="G4")
        ax1.plot(xecenter,data[code][w]["energy"],'g--',drawstyle='steps-mid',lw=lw,label=code)
        ax1.axvline(x=vertx,ymin=0,ymax=ymax,linestyle ="--",lw=0.8,color ='black')

        relativerrdiff=np.sqrt(data["G4"][w]["energy_relativerr"]**2+data[code][w]["energy_relativerr"]**2)
        errormax=(difference_energy+1)*relativerrdiff

        ax2.plot(xecenter,3*errormax,'g--',lw=0.5,drawstyle='steps-mid')
        ax2.plot(xecenter,-3*errormax,'g--',lw=0.5,drawstyle='steps-mid')
        ax2.plot([-10,100],[0,0],'k:',lw=0.5)
        ax2.plot(xecenter,difference_energy,'g-')

        ax2.set_xlabel('neutron energy (eV)',fontsize=16)
        ax1.set_ylabel('counts (a.u.)',fontsize=16)
        ax2.set_ylabel("relative difference",fontsize=16)

        ax1.set_xscale("linear")
        ax2.set_xscale("linear") #linear, log
        ax1.set_yscale("linear") #linear, log
        ax2.set_yscale("linear") #linear, log

        ax1.minorticks_on()
        ax2.minorticks_on()
        ax12 = ax1.secondary_yaxis("right")
        ax13 = ax1.secondary_xaxis("top")
        ax22 = ax2.secondary_yaxis("right")
        ax23 = ax2.secondary_xaxis("top")
        ax12.minorticks_on()
        ax13.minorticks_on()
        ax22.minorticks_on()
        ax23.minorticks_on()

        if yticksbool:
            ax12.set_yticks(yticks)
        ax2.set_yticks([-0.2,-0.1,0,0.1,0.2])

        majorticklength=6
        minorticklenght=3
        ax1.tick_params(labelsize=14,length=majorticklength, width=0.9)
        ax1.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax2.tick_params(labelsize=14,length=majorticklength, width=0.9)
        ax2.tick_params(which='minor',length=minorticklenght, bottom=True)
        ax1.ticklabel_format(axis="y",style="sci",scilimits=(4,4),useOffset=False,useMathText=True)
        ax1.yaxis.get_offset_text().set_fontsize(14)
        ax12.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax13.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax22.tick_params(axis="y", direction="in",labelright=False,length=majorticklength, width=0.9)
        ax23.tick_params(axis="x", direction="in",labeltop=False,length=majorticklength, width=0.9)
        ax12.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax13.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)
        ax22.tick_params(axis="y", which='minor',direction="in",labelright=False,length=minorticklenght)
        ax23.tick_params(axis="x", which='minor',direction="in",labeltop=False,length=minorticklenght)

        ax2.grid(True, axis="y")

        ax1.legend(loc='best',fontsize=14,labelcolor='k',ncol=1)  # Add a legend.

        plot_name="Energy_"+w+"DBRC_"+str(energy)+"_"+str(temperature)+".pdf"

        plt.savefig(path+"/"+plot_name,dpi=500, bbox_inches = 'tight')
        plt.close()
        #plt.show()
