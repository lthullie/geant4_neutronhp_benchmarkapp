import uproot
import numpy as np


def readTripoli(mac_or_mic,path,filename,radius=30,n_groups=616):
    data={}
    if mac_or_mic=="Macroscopic":
        with uproot.open(path+"/Tripoli-4/"+mac_or_mic.replace("scopic","")+"/"+filename+".root") as file_t:
            histo_t=file_t["volume_0"]
        data["flux"]=histo_t.values()
        data["flux_err"]=histo_t.errors()
        helparray=data["flux"].copy()
        helparray[data["flux"]==0]=1
        data["flux_relativerr"]=data["flux_err"]/helparray
        data["x_low"]=histo_t.axis().edges()
        volume=4/3*np.pi*radius**3
        data["flux"]=data["flux"]/volume
        data["flux_err"]=data["flux_err"]/volume
    elif mac_or_mic=="Microscopic":
        datacsv=np.genfromtxt(path+"/Tripoli-4/"+mac_or_mic.replace("scopic","")+"/"+filename+"_"+str(n_groups)+".csv",delimiter=",")
        data["energy"]=datacsv[:-1,1]
        data["energy_err"]=np.sqrt(datacsv[:-1,1])
        helparray=data["energy"].copy()
        helparray[data["energy"]==0]=1
        data["energy_relativerr"]=data["energy_err"]/helparray
        data["x_e"]=datacsv[:,0]
        data["costheta"]=datacsv[:200,3]
        data["costheta_err"]=np.sqrt(datacsv[:200,3])
        helparray=data["costheta"].copy()
        helparray[data["costheta"]==0]=1
        data["costheta_relativerr"]=data["costheta_err"]/helparray
        data["x_c"]=datacsv[:201,2]
        scale_factor=1e8/datacsv[n_groups,3]
        for i in ["energy","energy_err","costheta","costheta_err"]:
            data[i]=data[i]*scale_factor
    return data


def readMCNP(mac_or_mic,path,filename,n_groups=616):
    data={}
    if mac_or_mic=="Macroscopic":
        x_low=[]
        flux=[]
        flux_relativerr=[]
        with open(path+"/MCNP6.2/"+mac_or_mic.replace("scopic","")+"/"+filename+".m") as file_m:
            reading = [False,False]
            for line in file_m:
                if "et" in line:
                    reading[0]=True
                    line = file_m.readline()
                if "t        0" in line:
                    reading[0] = False
                if "vals" in line:
                    reading[1]=True
                    line=file_m.readline()
                if "tfc" in line:
                    reading[1]=False
                if reading[0]:
                    splitted_line=line.split()
                    for i in range(len(splitted_line)):
                        x_low.append(float(splitted_line[i]))
                if reading[1]:
                    splitted_line=line.split()
                    for i in range(int(len(splitted_line)/2)):
                        flux.append(float(splitted_line[i*2]))
                        flux_relativerr.append(float(splitted_line[i*2+1]))
        data["flux"]=np.array(flux[1:-1])
        data["flux_relativerr"]=np.array(flux_relativerr[1:-1])
        data["flux_err"]=data["flux"]*data["flux_relativerr"]
        data["x_low"]=np.array(x_low)
    elif mac_or_mic=="Microscopic":
        datacsv=np.genfromtxt(path+"/MCNP6.2/"+mac_or_mic.replace("scopic","")+"/"+filename+"_"+str(n_groups)+".csv",delimiter=",")
        data["energy"]=datacsv[:-1,1]
        data["energy_err"]=np.sqrt(datacsv[:-1,1])
        data["energy_relativerr"]=data["energy_err"]/data["energy"]
        data["x_e"]=datacsv[:,0]
        data["costheta"]=datacsv[:200,3]
        data["costheta_err"]=np.sqrt(datacsv[:200,3])
        data["costheta_relativerr"]=data["costheta_err"]/data["costheta"]
        data["x_c"]=datacsv[:201,2]
    return data


def readGeant(mac_or_mic,path,number_of_events):
    data={}
    if mac_or_mic=="Macroscopic":
        with uproot.open(path+"/Results.root") as file_g:
            histo_g=file_g["CellFlux"]
            data["flux"]=histo_g.values()
            data["flux_err"]=histo_g.errors()
            helparray=data["flux"].copy()
            helparray[data["flux"]==0]=1
            data["flux_relativerr"]=data["flux_err"]/helparray
            data["x_low"]=histo_g.axis().edges()
            data["flux"]=data["flux"]*100/number_of_events
            data["flux_err"]=data["flux_err"]*100/number_of_events
    elif mac_or_mic=="Microscopic":
        with uproot.open(path+"/Results.root") as file_g:
            histoe_g=file_g["Energy"]
            histocos_g=file_g["CosineAngle"]
        data["energy"]=histoe_g.values()
        data["energy_err"]=histoe_g.errors()
        helparray=data["energy"].copy()
        helparray[data["energy"]==0]=1
        data["energy_relativerr"]=data["energy_err"]/helparray
        data["x_e"]=histoe_g.axis().edges()
        data["costheta"]=histocos_g.values()
        data["costheta_err"]=histocos_g.errors()
        helparray=data["costheta"].copy()
        helparray[data["costheta"]==0]=1
        data["costheta_relativerr"]=data["costheta_err"]/helparray
        data["x_c"]=histocos_g.axis().edges()
        scale_factor=1e8/number_of_events
        for i in ["energy","energy_err","costheta","costheta_err"]:
            data[i]=data[i]*scale_factor
    return data

def readTripoliDBRC(path_name,n_groups=300):
    data={}
    datacsv=np.genfromtxt(path_name+".csv",delimiter=",")
    data["energy"]=datacsv[:-1,1]
    data["energy_err"]=np.sqrt(datacsv[:-1,1])
    helparray=data["energy"].copy()
    helparray[data["energy"]==0]=1
    data["energy_relativerr"]=data["energy_err"]/helparray
    data["x_e"]=datacsv[:,0]

    data["updown"]=datacsv[:4,2]
    data["updown_err"]=np.sqrt(datacsv[:4,2])
    helparray=data["updown"].copy()
    helparray[data["updown"]==0]=1
    data["updown_relativerr"]=data["updown_err"]/helparray
    data["x_ud"]=np.array([0.,6.52,20.2,36.25,1000000])*1e-6

    scale_factor=1e8/datacsv[n_groups,3]
    for i in ["energy","energy_err"]:
        data[i]=data[i]*scale_factor
    return data

def readGeantDBRC(path_name,number_of_events):
    data={}
    with uproot.open(path_name) as file_g:
        histoe_g=file_g["Energy"]
        histoud_g=file_g["UpAndDown"]
    data["energy"]=histoe_g.values()
    data["energy_err"]=histoe_g.errors()
    helparray=data["energy"].copy()
    helparray[data["energy"]==0]=1
    data["energy_relativerr"]=data["energy_err"]/helparray
    data["x_e"]=histoe_g.axis().edges()

    data["updown"]=histoud_g.values()
    data["updown_err"]=histoud_g.errors()
    helparray=data["updown"].copy()
    helparray[data["updown"]==0]=1
    data["updown_relativerr"]=data["updown_err"]/helparray
    data["x_ud"]=histoud_g.axis().edges()

    scale_factor=1e8/number_of_events
    for i in ["energy","energy_err"]:
        data[i]=data[i]*scale_factor
    return data
