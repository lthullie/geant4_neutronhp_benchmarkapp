import argparse
import numpy as np
import json
import os
import modules.reader as r
import modules.plotter as p
import modules.reporter as report



parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
parser.add_argument("-v","--version",default="11.00.03",
help="version of your Geant4")
parser.add_argument("-r","--radius",default=30,type=int,
help="radius (in cm) of the sphere in Macroscopic benchmark")
parser.add_argument("-nEMac","--nEventMacro",default=1e7,type=int,help="number of particles in Macroscopic benchmark")
parser.add_argument("-nEMic","--nEventMicro",default=1e8,type=int,help="number of particles in Microscopic benchmark")
parser.add_argument("-rel","--relativelimit",default=3,type=float,
help="relative limit of the analysis, see if difference is less than relativelimit*sigma+absolutelimit")
parser.add_argument("-abs","--absolutelimit",default=0.02,type=float,
help="absolute limit of the analysis, see if difference is less than relativelimit*sigma+absolutelimit")
args=parser.parse_args()

callout=[]
material_descriptions=[]

scriptpath=os.path.abspath(os.path.dirname(__file__))
databasepath=scriptpath.replace("Scripts","Database")
version=args.version
relativelimit=args.relativelimit
absolutelimit=args.absolutelimit

with open(scriptpath+"/auxiliary/librarydatabase.json") as jsonfile:
    database=json.load(jsonfile)
    libraries=database[version]

document=report.createtitlepage(databasepath,version)
for mac_or_mic in ["Macroscopic","Microscopic"]:
    if mac_or_mic=="Macroscopic":
        number_of_events=args.nEventMacro
        radius=args.radius
        databasekey="Sphere"+str(int(radius))+"cm"
        spheredir=databasekey+"/"
    elif mac_or_mic=="Microscopic":
        number_of_events=args.nEventMicro
        radius=1
        databasekey="ThinCylinder"
        spheredir=""

    exponent=int(np.log10(number_of_events))

    with open(scriptpath+"/auxiliary/matarialdatabase.json") as jsonfile:
        database=json.load(jsonfile)
        materials=database[databasekey]

    report.createheaderpage(mac_or_mic,document,scriptpath,exponent,radius)
    for TSLibrary in libraries:
        for librarydir in libraries[TSLibrary]:
            report.createlibrarypage(document,TSLibrary,librarydir)
            basepath=databasepath+"/Geant4/Geant4."+version+"/"+mac_or_mic.replace("scopic","")+"/"+spheredir+librarydir
            for material in materials:
                data={"G4":{},"T4":{},"M6":{}}
                if material["TSLibrary"]!=TSLibrary:
                    continue

                #setting new dir name
                if mac_or_mic=="Macroscopic":
                    newdir=(librarydir+"_"+material["material"]+"_"+str(radius)+"CM_"+
                    str(material["temperature"])+"K_"+str(material["energy"])+"eV"
                    +"/1E"+str(exponent))
                elif mac_or_mic=="Microscopic":
                    newdir=(librarydir+"_"+material["material"]+"_"+
                    str(material["temperature"])+"K_"+str(material["energy"])+"eV"
                    +"/1E"+str(exponent))
                if not(os.path.isdir(basepath+"/"+newdir)):
                    callout.append("The directory "+newdir+" does not exist.")
                    continue
                workingdir=basepath+"/"+newdir
                print("\n\nWorking on "+newdir+"...")

                #finding all suitable datasets for comparison
                if not(os.path.exists(workingdir+"/Results.root")):
                    callout.append("The directory "+newdir+" does not contain any results.")
                    continue
                if material["T4_file"]=="":
                    callout.append("There is no T4_file for "+newdir+'.')
                    data.pop("T4")
                if material["M6_file"]=="":
                    callout.append("There is no M6_file for "+newdir+'.')
                    data.pop("M6")
                if len(data.keys())<=1:
                    callout.append("There is nothing to compare for "+newdir+'.')
                    continue
                if not(material["material"] in material_descriptions):
                    material_descriptions.append(material["material"])

                #reading files
                if "T4" in data:
                    data["T4"]=r.readTripoli(mac_or_mic,databasepath,material["T4_file"],radius)
                if "M6" in data:
                    data["M6"]=r.readMCNP(mac_or_mic,databasepath,material["M6_file"])
                data["G4"]=r.readGeant(mac_or_mic,workingdir,number_of_events)

                #statistics
                statistictext=report.dostatistics(mac_or_mic,data,material["material"],
                workingdir,relativelimit,absolutelimit)

                #plot files
                xcenter=p.testandgetx(mac_or_mic,data,newdir)
                if xcenter[0]:
                    callout.append(xcenter[1])
                    continue
                if mac_or_mic=="Macroscopic":
                    p.plotmacro(xcenter[1],data,workingdir,material["material"])
                if mac_or_mic=="Microscopic":
                    p.plotmicro(xcenter[1],xcenter[2],data,workingdir,material["material"])

                #create page
                report.createpage(mac_or_mic,statistictext,document,workingdir,material,data)

report.createendingpage(document,scriptpath,material_descriptions)
os.chdir(databasepath+"/Geant4/Geant4."+version)
os.system("pdflatex "+"Geant4."+version+"NeutronHPBenchmark")
os.system("bibtex "+"Geant4."+version+"NeutronHPBenchmark")
os.system("pdflatex "+"Geant4."+version+"NeutronHPBenchmark")
os.system("pdflatex "+"Geant4."+version+"NeutronHPBenchmark")

for call in callout:
    print(call)
callout.append("JOB DONE!")
exit()
