import os
import json
import math
import sys
import argparse
import numpy as np
import modules.reader as r
import modules.plotter as p
import modules.reporter as report
import subprocess
import time

parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
parser.add_argument("-v","--version",default="11.00.03",
help="version of your Geant4 you want to test")
parser.add_argument("-appv","--ApplicationVersion",default="11.00.03",choices=["11.00.03"],
help="version of the application, that should be used, currently there is one tested application for version 11.00.03, which should work for all versions 11 so far")
parser.add_argument("-refl","--referenceLibrary",default="ENDFB71",choices=["ENDFB71"],
help="library used in the reference code Tripoli-4")
parser.add_argument("-nhp","--NeutronHPdatafile",default="G4NDL45",
choices=["G4NDL45","G4NDL46","G4NDL47","JEFF33","JEFF32","ENDFB80","ENDFB71","BROND31","JENDL4u"],
help="data file with Geant4 NeutronHP library, must be set in geant4.sh on your computer")
parser.add_argument("-nE","--nEvents",default=1e8,type=int,help="number of particles in benchmark")
parser.add_argument("-nT","--numberOfThreads",default=0,type=int,
help="number of threads used for simulation in Geant4, default 0 for automatic choice")

args=parser.parse_args()

scriptpath=os.path.abspath(os.path.dirname(__file__))
databasepath=scriptpath.replace("Scripts","Database")
applicationpath=scriptpath.replace("Scripts","Apps")
appversion=args.ApplicationVersion
version=args.version
refLibrary=args.referenceLibrary
libraryHP=args.NeutronHPdatafile
librarydir=refLibrary+"_"+libraryHP
numofthreads=args.numberOfThreads
number_of_events=args.nEvents
exponent=int(np.log10(number_of_events))

verboscmd="/run/verbose 2"
controlcmd="/control/verbose 2"
numofthreadscmd="/run/numberOfThreads "
detmatcmd="/testhadr/det/setMatSpecial "
initializecmd="/run/initialize"
particlehpcmd1="/process/had/particle_hp/skip_missing_isotopes true"
particlehpcmd2="/process/had/particle_hp/do_not_adjust_final_state true"
specialcommand={"w":"/process/had/particle_hp/use_DBRC true","wout":"/process/had/particle_hp/SVT_E_max 210 eV"}
gpscmd="""/gps/particle neutron
/gps/pos/type Point
/gps/pos/centre 0. 0. -99.9 cm
/gps/direction 0 0 1"""
runcmd1="/run/printProgress "
runcmd2="/run/beamOn "
delimiter="#\n"


if not os.path.exists(applicationpath+"/"+"MicroscopicDBRC"+"_G4."+version+"-build"):
    os.system("mkdir "+applicationpath+"/"+"MicroscopicDBRC"+"_G4."+version+"-build")

with open(scriptpath+"/auxiliary/librarydatabaseDBRC.json","r+") as jsonfile:
    if jsonfile.read(1):
        jsonfile.seek(0)
        database=json.load(jsonfile)
        if not(version in database.keys()):
            database[version]={}
            database[version][refLibrary]=[libraryHP]
        elif not(refLibrary in database[version].keys()):
            database[version][refLibrary]=[libraryHP]
        elif not(libraryHP in database[version][refLibrary]):
            database[version][refLibrary].append(libraryHP)
        jsonfile.seek(0)
        jsonfile.truncate()
        json.dump(database,jsonfile, indent=2)
    else:
        database={}
        database[version]={}
        database[version][refLibrary]=[libraryHP]
        json.dump(database,jsonfile, indent=2)

if not os.path.exists(databasepath+"/Geant4/Geant4."+version):
    os.system("mkdir "+databasepath+"/Geant4/Geant4."+version)
if not os.path.exists(databasepath+"/Geant4/Geant4."+version+"/DBRC"):
    os.system("mkdir "+databasepath+"/Geant4/Geant4."+version+"/DBRC")
basepath=databasepath+"/Geant4/Geant4."+version+"/DBRC"

if(not(os.path.isdir(basepath+"/"+librarydir))):
    os.system("mkdir "+basepath+"/"+librarydir)
librarypath=basepath+"/"+librarydir

with open(scriptpath+"/auxiliary/matarialdatabaseDBRC.json") as jsonfile:
    database=json.load(jsonfile)
    materials=database["DBRC"]

for material in materials:
    if material["library"]!=refLibrary:
        continue
    newdir=material["material"]
    if(not(os.path.isdir(librarypath+"/"+newdir))):
        os.system("mkdir "+librarypath+"/"+newdir)
    newdir=newdir+"/1E"+str(exponent)
    if not(os.path.isdir(librarypath+"/"+newdir)):
        os.system("mkdir "+librarypath+"/"+newdir)
    for energy in material["energies"]:
        energycmd="/gps/ene/mono "+str(energy)+" eV"
        new_file_content=""
        with open(applicationpath+"/MicroscopicDBRC."+appversion+"/src/RunAction.cc","r") as file:
            for line in file:
                new_file_content+=line
                if ("// Creating histograms" in line):
                    if(energy==6.52):
                        new_file_content+='analysisManager->CreateH1("Energy","Scattered energy",300,5*eV,8*eV);'+"\n"
                    elif(energy==20.2):
                        new_file_content+='analysisManager->CreateH1("Energy","Scattered energy",300,18.7*eV,21.7*eV);'+"\n"
                    elif(energy==36.25):
                        new_file_content+='analysisManager->CreateH1("Energy","Scattered energy",300,34.7*eV,37.7*eV);'+"\n"
                    line=file.readline()
        os.system("rm "+applicationpath+"/MicroscopicDBRC."+appversion+"/src/RunAction.cc")
        with open(applicationpath+"/MicroscopicDBRC."+appversion+"/src/RunAction.cc","w+") as newfile:
            newfile.write(new_file_content)
        os.chdir(applicationpath+"/"+"MicroscopicDBRC"+"_G4."+version+"-build")
        os.system("cmake ../MicroscopicDBRC."+appversion)
        os.system("make")
        print("\n"+"Application prepared for "+str(energy)+" eV."+"\n\n")
        for w in ["wout","w"]:
            workingdir=librarypath+"/"+newdir+"/"+w+"DBRC"
            if not(os.path.isdir(workingdir)):
                os.system("mkdir "+workingdir)
            os.chdir(workingdir)
            for temperature in material["temperatures"]:
                resultname="Results_"+str(energy)+"_"+str(temperature)+".root"
                outputname="output_"+str(energy)+"_"+str(temperature)+".txt"
                runcmd=applicationpath+"/MicroscopicDBRC"+"_G4."+version+"-build/Microscopic "+"myrun.mac > "+outputname
                with open(workingdir+"/myrun.mac",'w') as myrunfile:
                    myrunfile.write(verboscmd+'\n')
                    myrunfile.write(controlcmd+'\n')
                    myrunfile.write(delimiter)
                    if numofthreads>0:
                        myrunfile.write(numofthreadscmd+str(numofthreads)+'\n')
                        myrunfile.write(delimiter)
                    myrunfile.write(detmatcmd+material["G4_name"]+" "+
                    str(material["density"])+" g/cm3 "+
                    str(temperature)+" kelvin "+
                    str(material["pressure"])+" atmosphere"+'\n')
                    myrunfile.write(delimiter)
                    myrunfile.write(initializecmd+'\n')
                    myrunfile.write(delimiter)
                    myrunfile.write(particlehpcmd1+'\n')
                    myrunfile.write(particlehpcmd2+'\n')
                    myrunfile.write(specialcommand[w]+'\n')
                    myrunfile.write(delimiter)
                    myrunfile.write(gpscmd+'\n')
                    myrunfile.write(energycmd+'\n')
                    myrunfile.write(delimiter)
                    myrunfile.write(runcmd1+str(int(number_of_events/10))+'\n')
                    myrunfile.write(runcmd2+str(int(number_of_events)))
                if not(os.path.exists(workingdir+"/"+resultname)):
                    print(workingdir)
                    os.chdir(workingdir)
                    print("\n\nCalculating for "+str(temperature)+" K in "+w+"DBRC"+"\n"+"...")
                    print(runcmd)
                    os.system(runcmd)
                    print("Simulation for "+str(temperature)+" K in "+w+"DBRC"+" finished.")
                    os.system("mv Results.root "+resultname)


callout=[]

with open(scriptpath+"/auxiliary/librarydatabaseDBRC.json") as jsonfile:
    database=json.load(jsonfile)
    libraries=database[version]

document=report.createtitlepageDBRC(basepath,version)
report.createheaderpageDBRC(document,scriptpath,exponent)

for library in libraries:
    for libraryHP in libraries[library]:
        librarydir=library+"_"+libraryHP
        report.createlibrarypageDBRC(document,library,libraryHP)
        basepath=databasepath+"/Geant4/Geant4."+version+"/DBRC/"+librarydir
        for material in materials:
            document.write("\\subsection{"+material["material"]+"}")
            if (library!=material["library"]):
                continue
            materialpath=basepath+"/"+material["material"]+"/1E"+str(exponent)
            if not(os.path.isdir(materialpath)):
                print(materialpath)
                callout.append("The directory "+material["material"]+"/1E"+str(exponent)+" does not exist.")
                continue
            upanddowncsv=np.zeros((len(material["energies"])*len(material["temperatures"])*2,12))
            n_t=len(material["temperatures"])
            i_e=-1
            for energy in material["energies"]:
                i_e+=1
                i_t=-1
                for temperature in material["temperatures"]:
                    i_t+=1
                    data={"G4":{},"T4":{}}
                    for w in ["wout","w"]:
                        file_name=materialpath+"/"+w+"DBRC"+"/Results_"+str(energy)+"_"+str(temperature)+".root"
                        if not(os.path.isdir(materialpath+"/"+w+"DBRC")):
                            callout.append("The file "+"Results_"+str(energy)+"_"+str(temperature)+".root"+" in "+"/"+w+"DBRC"+" does not exist.")
                            continue
                        data["G4"][w]=r.readGeantDBRC(file_name,number_of_events)
                        data["T4"][w]=r.readTripoliDBRC(databasepath+"/Tripoli-4/DBRC/"+material["T4_files_"+w][i_e*n_t+i_t])
                        data["G4"][w]["energy"]*=(np.sum(data["T4"][w]["energy"])/np.sum(data["G4"][w]["energy"]))
                        xecenter=np.zeros((data["G4"][w]["x_e"].size-1))
                        for j in range(data["G4"][w]["x_e"].size-1):
                            xecenter[j]=(data["G4"][w]["x_e"][j]+data["G4"][w]["x_e"][j+1])/2*1e6
                        i_c=-1
                        for code in ["G4","T4"]:
                            i_c+=1
                            if(energy==6.52):
                                down=np.sum(data[code][w]["updown"][:1])
                                up=np.sum(data[code][w]["updown"][1:])
                            elif(energy==20.2):
                                down=np.sum(data[code][w]["updown"][:2])
                                up=np.sum(data[code][w]["updown"][2:])
                            elif(energy==36.25):
                                down=np.sum(data[code][w]["updown"][:3])
                                up=np.sum(data[code][w]["updown"][3:])
                            emean=np.sum(xecenter*data[code][w]["energy"])/np.sum(data[code][w]["energy"])
                            emean_err=np.sqrt(np.sum(data[code][w]["energy_err"]**2*(xecenter-emean)**2/(np.sum(data[code][w]["energy"]))**2))
                            elastic=np.sum(data[code][w]["updown"])
                            if w=="wout":
                                upanddowncsv[2*(i_e*n_t+i_t)+i_c,0]=elastic/number_of_events*100
                                upanddowncsv[2*(i_e*n_t+i_t)+i_c,1]=down/elastic*100
                                upanddowncsv[2*(i_e*n_t+i_t)+i_c,2]=up/elastic*100
                                upanddowncsv[2*(i_e*n_t+i_t)+i_c,3]=up/elastic*np.sqrt(1/elastic+1/up)*100
                                upanddowncsv[2*(i_e*n_t+i_t)+i_c,4]=emean
                                upanddowncsv[2*(i_e*n_t+i_t)+i_c,5]=emean_err
                            elif w=="w":
                                upanddowncsv[2*(i_e*n_t+i_t)+i_c,6]=elastic/number_of_events*100
                                upanddowncsv[2*(i_e*n_t+i_t)+i_c,7]=down/elastic*100
                                upanddowncsv[2*(i_e*n_t+i_t)+i_c,8]=up/elastic*100
                                upanddowncsv[2*(i_e*n_t+i_t)+i_c,9]=up/elastic*np.sqrt(1/elastic+1/up)*100
                                upanddowncsv[2*(i_e*n_t+i_t)+i_c,10]=emean
                                upanddowncsv[2*(i_e*n_t+i_t)+i_c,11]=emean_err
                    for w in ["wout","w"]:
                        p.plotmicroDBRC(xecenter,data,energy,temperature,w,materialpath)
                report.createenergypageDBRC(document,energy,materialpath,material)
            np.savetxt(materialpath+"/table_results.csv",upanddowncsv,delimiter=',')
            report.createtablepageDBRC(document,material,upanddowncsv)


report.createendingpageDBRC(document,scriptpath)
os.chdir(databasepath+"/Geant4/Geant4."+version+"/"+"DBRC")
os.system("pdflatex "+"Geant4."+version+"DBRCBenchmark")
os.system("bibtex "+"Geant4."+version+"DBRCBenchmark")
os.system("pdflatex "+"Geant4."+version+"DBRCBenchmark")
os.system("pdflatex "+"Geant4."+version+"DBRCBenchmark")

callout.append("JOB DONE!")
for call in callout:
    print(call)
exit()
