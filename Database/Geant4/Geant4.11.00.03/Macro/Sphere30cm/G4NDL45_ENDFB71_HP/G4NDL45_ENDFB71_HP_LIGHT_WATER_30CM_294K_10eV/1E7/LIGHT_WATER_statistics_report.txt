The condition is: |relative difference|$\leq3.0\cdot \sigma+0.02$.

In comparison with T4 the condition is not met for 1 non-zero bins,                which is 0.3322 \% of all non-zero bins.\\
These are the ranges (in eV), where the condition is not met:\\
0.00057544-0.00060256.\\
The sum of squares (G4-T4)$^2$ is 4.292e-13. The sum of relative difference |G4-T4|/T4 is 4.944.\\
The weighted mean of relative difference |G4-T4|/T4 in non-zero bins is 1.093\%.

\vspace{5mm}

In comparison with M6 the condition is not met for 3 non-zero bins,                which is 0.9967 \% of all non-zero bins.\\
These are the ranges (in eV), where the condition is not met:\\
1.51356e-05-1.58489e-05, 1.99526e-05-2.0893e-05, 0.00057544-0.00060256.\\
The sum of squares (G4-M6)$^2$ is 4.883e-13. The sum of relative difference |G4-M6|/M6 is 6.29.\\
The weighted mean of relative difference |G4-M6|/M6 in non-zero bins is 1.173\%.