The condition is: |relative difference|$\leq3.0\cdot \sigma+0.02$.

In comparison with T4 the condition is not met for 10 non-zero bins,                which is 3.322 \% of all non-zero bins.\\
These are the ranges (in eV), where the condition is not met:\\
0.00301995-0.00316228, 0.0057544-0.0060256, 3.16228-3.63078, 3.98107-5.01187.\\
The sum of squares (G4-T4)$^2$ is 1.472e-12. The sum of relative difference |G4-T4|/T4 is 30.07.\\
The weighted mean of relative difference |G4-T4|/T4 in non-zero bins is 0.4378\%.

\vspace{5mm}

In comparison with M6 the condition is not met for 22 non-zero bins,                which is 7.309 \% of all non-zero bins.\\
These are the ranges (in eV), where the condition is not met:\\
2.63027e-05-2.75423e-05, 4.57088e-05-4.7863e-05, 0.00199526-0.0020893, 0.00301995-0.00316228, 0.00457088-0.0047863, 0.0057544-0.0060256, 0.00758578-0.00794328, 0.0109648-0.0114815, 3.16228-3.63078, 4.16869-5.49541, 6.91831-8.70964.\\
The sum of squares (G4-M6)$^2$ is 1.825e-12. The sum of relative difference |G4-M6|/M6 is 113.6.\\
The weighted mean of relative difference |G4-M6|/M6 in non-zero bins is 0.493\%.