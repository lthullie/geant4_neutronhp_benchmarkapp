\contentsline {chapter}{\numberline {1}Macroscopic}{2}{chapter.1}%
\contentsline {section}{\numberline {1.1}G4NDL45\_ENDFB71\_HP}{3}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}ALUMINIUM\_METAL 294 K}{4}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}BERYLLIUM\_METAL 296 K}{5}{subsection.1.1.2}%
\contentsline {subsection}{\numberline {1.1.3}BERYLLIUM\_OXIDE 294 K}{6}{subsection.1.1.3}%
\contentsline {subsection}{\numberline {1.1.4}GRAPHITE 296 K}{7}{subsection.1.1.4}%
\contentsline {subsection}{\numberline {1.1.5}LIGHT\_WATER 294 K}{8}{subsection.1.1.5}%
\contentsline {subsection}{\numberline {1.1.6}PARA\_HYDROGEN 20 K}{9}{subsection.1.1.6}%
\contentsline {subsection}{\numberline {1.1.7}POLYETHYLENE 296 K}{10}{subsection.1.1.7}%
\contentsline {section}{\numberline {1.2}G4NDL47\_ENDFB80\_HP}{11}{section.1.2}%
\contentsline {chapter}{\numberline {2}Microscopic}{12}{chapter.2}%
\contentsline {section}{\numberline {2.1}G4NDL45\_ENDFB71\_HP}{13}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}BERYLLIUM\_METAL 296 K}{14}{subsection.2.1.1}%
\contentsline {subsubsection}{Energy}{14}{section*.2}%
\contentsline {subsubsection}{Angle}{15}{section*.3}%
\contentsline {subsection}{\numberline {2.1.2}BERYLLIUM\_OXIDE 294 K}{16}{subsection.2.1.2}%
\contentsline {subsubsection}{Energy}{16}{section*.4}%
\contentsline {subsubsection}{Angle}{17}{section*.5}%
\contentsline {subsection}{\numberline {2.1.3}GRAPHITE 296 K}{18}{subsection.2.1.3}%
\contentsline {subsubsection}{Energy}{18}{section*.6}%
\contentsline {subsubsection}{Angle}{19}{section*.7}%
\contentsline {subsection}{\numberline {2.1.4}LIGHT\_WATER 294 K}{20}{subsection.2.1.4}%
\contentsline {subsubsection}{Energy}{20}{section*.8}%
\contentsline {subsubsection}{Angle}{21}{section*.9}%
\contentsline {subsection}{\numberline {2.1.5}PARA\_HYDROGEN 20 K}{22}{subsection.2.1.5}%
\contentsline {subsubsection}{Energy}{22}{section*.10}%
\contentsline {subsubsection}{Angle}{23}{section*.11}%
\contentsline {subsection}{\numberline {2.1.6}POLYETHYLENE 296 K}{24}{subsection.2.1.6}%
\contentsline {subsubsection}{Energy}{24}{section*.12}%
\contentsline {subsubsection}{Angle}{25}{section*.13}%
\contentsline {section}{\numberline {2.2}G4NDL47\_ENDFB80\_HP}{26}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}POLYETHYLENE 77 K}{27}{subsection.2.2.1}%
\contentsline {subsubsection}{Energy}{27}{section*.14}%
\contentsline {subsubsection}{Angle}{28}{section*.15}%
\contentsline {subsection}{\numberline {2.2.2}POLYETHYLENE 294 K}{29}{subsection.2.2.2}%
\contentsline {subsubsection}{Energy}{29}{section*.16}%
\contentsline {subsubsection}{Angle}{30}{section*.17}%
\contentsline {chapter}{\numberline {A}Material descriptions}{32}{appendix.A}%
\contentsline {subsubsection}{ALUMINIUM\_METAL}{32}{section*.19}%
\contentsline {subsubsection}{BERYLLIUM\_METAL}{32}{section*.20}%
\contentsline {subsubsection}{BERYLLIUM\_OXIDE}{32}{section*.21}%
\contentsline {subsubsection}{GRAPHITE}{32}{section*.22}%
\contentsline {subsubsection}{LIGHT\_WATER}{32}{section*.23}%
\contentsline {subsubsection}{PARA\_HYDROGEN}{33}{section*.24}%
\contentsline {subsubsection}{POLYETHYLENE}{33}{section*.25}%
