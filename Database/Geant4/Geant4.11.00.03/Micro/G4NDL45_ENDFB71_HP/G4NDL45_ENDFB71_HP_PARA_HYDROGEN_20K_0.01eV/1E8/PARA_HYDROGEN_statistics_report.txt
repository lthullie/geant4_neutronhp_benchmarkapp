The condition is: |relative difference|$\leq3.0\cdot \sigma+0.02$.

In comparison with T4 for energy the condition is not met for 31 non-zero bins,                which is 17.51 \% of all non-zero bins.\\
These are the ranges (in eV), where the condition is not met:\\
1.38038e-05-1.44544e-05, 2.29087e-05-2.63027e-05, 6.30957e-05-6.91831e-05, 0.000120226-0.000138038, 0.000190546-0.000218776, 0.000239883-0.000275423, 0.000288403-0.000331131, 0.000346737-0.000363078, 0.00047863-0.000524807, 0.00138038-0.00144544, 0.00199526-0.0020893, 0.00218776-0.00251189, 0.00288403-0.00316228, 0.00549541-0.0057544, 0.00630957-0.00691831.\\
The sum of squares (G4-T4)$^2$ is 3.354e+10. The sum of relative difference |G4-T4|/T4 is 8.271.\\
The weighted mean of relative difference |G4-T4|/T4 in non-zero bins is 1.216\%.

\vspace{5mm}

In comparison with M6 for energy the condition is not met for 34 non-zero bins,                which is 19.21 \% of all non-zero bins.\\
These are the ranges (in eV), where the condition is not met:\\
1.25893e-05-1.31826e-05, 1.38038e-05-1.44544e-05, 2.18776e-05-2.63027e-05, 6.30957e-05-6.91831e-05, 0.000120226-0.000125893, 0.000131826-0.000138038, 0.000190546-0.000218776, 0.000239883-0.000275423, 0.000288403-0.000331131, 0.000346737-0.000363078, 0.00047863-0.000524807, 0.00138038-0.00144544, 0.00199526-0.0020893, 0.00218776-0.00251189, 0.00288403-0.00331131, 0.00549541-0.0057544, 0.00630957-0.00691831, 0.0301995-0.0316228.\\
The sum of squares (G4-M6)$^2$ is 3.173e+10. The sum of relative difference |G4-M6|/M6 is 7.369.\\
The weighted mean of relative difference |G4-M6|/M6 in non-zero bins is 1.205\%.

In comparison with T4 for angle the condition is not met for 40 non-zero bins,                which is 20 \% of all non-zero bins.\\
These are the ranges (in $\mu$), where the condition is not met:\\
-0.97--0.96, -0.92--0.91, -0.57--0.56, -0.2--0.19, -0.1--0.06, -0.02--0.01, 0.12-0.13, 0.16-0.17, 0.62-0.64, 0.65-0.66, 0.67-0.72, 0.73-0.83, 0.89-1.\\
The sum of squares (G4-T4)$^2$ is 3.078e+10. The sum of relative difference |G4-T4|/T4 is 5.046.\\
The weighted mean of relative difference |G4-T4|/T4 in non-zero bins is 2.145\%.

\vspace{5mm}

In comparison with M6 for angle the condition is not met for 118 non-zero bins,                which is 59 \% of all non-zero bins.\\
These are the ranges (in $\mu$), where the condition is not met:\\
-1--0.89, -0.84--0.83, -0.81--0.65, -0.58--0.54, -0.52--0.47, -0.45--0.43, -0.34--0.27, -0.26--0.24, -0.18--0.09, -0.03-0.07, 0.11-0.16, 0.17-0.19, 0.21-0.22, 0.25-0.27, 0.3-0.32, 0.37-0.38, 0.59-0.69, 0.7-0.86, 0.88-1.\\
The sum of squares (G4-M6)$^2$ is 1.271e+11. The sum of relative difference |G4-M6|/M6 is 14.61.\\
The weighted mean of relative difference |G4-M6|/M6 in non-zero bins is 4.494\%.