The condition is: |relative difference|$\leq3.0\cdot \sigma+0.02$.

In comparison with T4 for energy the condition is not met for 35 non-zero bins,                which is 15.84 \% of all non-zero bins.\\
These are the ranges (in eV), where the condition is not met:\\
5.01187e-05-5.24807e-05, 8.31764e-05-8.70964e-05, 0.00131826-0.00138038, 0.00263027-0.00275423, 0.00288403-0.00301995, 0.00660693-0.00758578, 0.00870964-0.00912011, 0.00954993-0.01, 0.0109648-0.0151356, 0.020893-0.0229087, 0.0263027-0.0316228, 0.0331131-0.0363078, 0.0380189-0.0549541, 0.0794328-0.0870964.\\
The sum of squares (G4-T4)$^2$ is 2.732e+09. The sum of relative difference |G4-T4|/T4 is 38.26.\\
The weighted mean of relative difference |G4-T4|/T4 in non-zero bins is 0.3233\%.

\vspace{5mm}

In comparison with M6 for energy the condition is not met for 36 non-zero bins,                which is 16.29 \% of all non-zero bins.\\
These are the ranges (in eV), where the condition is not met:\\
1.8197e-05-1.90546e-05, 3.46737e-05-3.63078e-05, 5.01187e-05-5.24807e-05, 0.00131826-0.00138038, 0.00263027-0.00275423, 0.00288403-0.00301995, 0.00660693-0.00691831, 0.00724436-0.00758578, 0.00870964-0.00912011, 0.00954993-0.01, 0.0109648-0.0151356, 0.020893-0.0229087, 0.0263027-0.0316228, 0.0331131-0.0363078, 0.0380189-0.0549541, 0.057544-0.060256, 0.0794328-0.0870964.\\
The sum of squares (G4-M6)$^2$ is 2.707e+09. The sum of relative difference |G4-M6|/M6 is 36.03.\\
The weighted mean of relative difference |G4-M6|/M6 in non-zero bins is 0.3175\%.

In comparison with T4 for angle the condition is not met for 19 non-zero bins,                which is 9.5 \% of all non-zero bins.\\
These are the ranges (in $\mu$), where the condition is not met:\\
0.65-0.66, 0.73-0.82, 0.91-1.\\
The sum of squares (G4-T4)$^2$ is 1.6e+08. The sum of relative difference |G4-T4|/T4 is 3.95.\\
The weighted mean of relative difference |G4-T4|/T4 in non-zero bins is 0.07405\%.

\vspace{5mm}

In comparison with M6 for angle the condition is not met for 33 non-zero bins,                which is 16.5 \% of all non-zero bins.\\
These are the ranges (in $\mu$), where the condition is not met:\\
0.5-0.54, 0.57-0.6, 0.71-0.73, 0.74-0.9, 0.92-1.\\
The sum of squares (G4-M6)$^2$ is 2.432e+08. The sum of relative difference |G4-M6|/M6 is 134.9.\\
The weighted mean of relative difference |G4-M6|/M6 in non-zero bins is 0.1138\%.